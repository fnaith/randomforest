/*
 * RandomForest.cpp
 *
 *  Created on: Jun 28, 2010
 *      Author: operry5
 */

#include "../h/RandomForest.h"
#include "../h/DataVar.h"
#include <iostream>
#include <fstream>
#include <sstream> 
#include <string>
#include <time.h>
#include <cstring>
#include <cstdlib>

using namespace std;

RandomForest::RandomForest(unsigned int size, unsigned int attrsPerTree, vector< vector<DataVar*> > *attrs, vector< vector<DataVar*> > *data):
					minPercentage(0),forest(),size(size),attributes(attrs),data(data),errorOnBuild(false),constructedWithPtrs(true){
	forest=new DecisionTree*[size];
	for(unsigned int i=0; i<size; i++){
		cout<< "creating tree #" <<i+1<<endl;
		//randomly select attrsPerTree attributes, and make tree with them:
		srand((unsigned int)time(NULL));
		vector<int> chosen;
		while(chosen.size()<attrsPerTree){
			int j=rand()% (attributes->size()-1); //draw a random attribute (last one is classifications thus -1)
			bool found=false;
			for(unsigned int k=0; k<chosen.size();k++)
				if(chosen.at(k)==j){
					found=true;
					break;
				}
			if(!found)
				chosen.push_back(j);
		}
		vector<vector <DataVar*> > *att=new vector<vector <DataVar*> >(),*dat=new vector<vector <DataVar*> >();
		//for(unsigned int j=0; j<data.size();j++)
		dat->resize(data->size());
		for(unsigned int j=0; j<attrsPerTree;j++){
			att->push_back(attributes->at(chosen.at(j)));
			for(unsigned int k=0; k<data->size(); k++)
				dat->at(k).push_back(data->at(k).at(chosen.at(j)));
		}
		att->push_back(attributes->back());
		for(unsigned int k=0; k<data->size(); k++)
			dat->at(k).push_back(data->at(k).back());

 		forest[i]=new DecisionTree(att,dat);
	}
}

RandomForest::RandomForest(unsigned int size, unsigned int attrsPerTree, const std::string& attrFile, const std::string& dataFile):
					minPercentage(0),forest(),size(size),attributes(),data(),errorOnBuild(false),constructedWithPtrs(false){
	readData(attrFile,dataFile);
	if(errorOnBuild)
		return;
	forest=new DecisionTree*[size];
	for(unsigned int i=0; i<size; i++){
		//randomly select attrsPerTree attributes, and make tree with them:
		srand((unsigned int)time(NULL));
		vector<int> chosen;
		while(chosen.size()<attrsPerTree){
			int j=rand()% attributes->size();
			bool found=false;
			for(unsigned int k=0; k<chosen.size();k++)
				if(chosen.at(k)==j){
					found=true;
					break;
				}
			if(!found)
				chosen.push_back(j);
		}
		vector<vector <DataVar*> > *att=new vector<vector <DataVar*> >(),*dat=new vector<vector <DataVar*> >();
		
		dat->resize(data->size());
		for(unsigned int j=0; j<attrsPerTree;j++){
			att->push_back(attributes->at(j));
			for(unsigned int k=0; k<data->size(); k++)
				dat->at(k).push_back(data->at(k).at(j));
		}
		att->push_back(attributes->back());
		forest[i]=new DecisionTree(att,dat);
	}
}

RandomForest::~RandomForest(){
	if(!constructedWithPtrs){
		for(unsigned int i=0; i<attributes->size(); i++){
			for(unsigned int j=0; j<attributes->at(i).size(); j++){
				delete(attributes->at(i).at(j));
			}
		}
		delete attributes;
		for(unsigned int i=0; i<data->size(); i++){
			for(unsigned int j=0; j<data->at(i).size(); j++){
				delete(data->at(i).at(j));
			}
		}
		delete data;
	}
	for(unsigned int i=0; i<this->size; i++){
		this->forest[i]->clearAtrNdata();
		delete this->forest[i];
	}
	delete[] forest;
}

RandomForest::RandomForest(const RandomForest& ){
	cout<< "ERROR: RandomForest use of copy constructor"<< endl;
}

RandomForest& RandomForest::operator=(const RandomForest& t){
	cout<< "ERROR: RandomForest use of operator="<< endl;
	RandomForest *f=new RandomForest(t);
	return *f;
}

/*
 * this initializes "attributes", "classifications" and "data".
 * a record in dataFile must be of the form: <val1>,<val2>,...,<LastVal>,<class>
 */
void RandomForest::readData(const std::string& attrFile, const std::string& dataFile){
	this->data->clear(); //in case a former 'read' has been interrupted.
	unsigned int ind=0,i=0; //this one helps determine which line gets token.
	vector<DataVar*> *dataLn;
	string line,word;
	DataVar* token;
	setAttrs(attrFile);
	if(errorOnBuild)
		return;
	int lineInd=0;	//just for error signaling.
	ifstream myfile(dataFile.c_str(),ifstream::in);
	if (myfile.is_open()){
		while (! myfile.eof() ){
			getline (myfile,line);
			lineInd++;
			ind=0;
			dataLn= new vector<DataVar*>();
			while(line.length()>0){
				if(line.at(0)==' ' || line.at(0)=='\t'){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				word=line.substr(0,line.find(',',0));
				if(word.length()==line.length()){
					line="";
					if(ind < attributes->size()-1){
						cout<< "missing data in record (line:" << lineInd << "). aborting"<<endl;
						delete dataLn;
						errorOnBuild=true;
						return;
					}
				}else{
					line=line.substr(line.find(',',0)+1);
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\t')
					word=word.substr(0,word.length()-1);

				if(attributes->at(ind).size()==1)//continuous variable
					token=new DataVar(NUMERIC,word);
				else{
					for(i=1; i<attributes->at(ind).size();i++)
						if(strcmp(word.c_str(),
								attributes->at(ind).at(i)->getStrVal().c_str())==0){
							token=attributes->at(ind).at(i);//TODO - what's going on here with operator= ??
							break; //for
						}
					if(i >= attributes->at(ind).size()){
						cout << "invalid value on " << word.c_str() <<" aborting."<<endl;
						delete dataLn;
						errorOnBuild=true;
						return;
					}
				}//token is now the next data element to add
				if(ind==attributes->size()){
					cout << "too many values in record (line:" << lineInd << "). aborting." <<endl;
					delete dataLn;
					errorOnBuild=true;
					return;
				}
				dataLn->push_back(token);
				ind++;
			}
			if(dataLn->size()>0)
				data->push_back(*dataLn);
			else
				dataLn->~vector();
		}
		myfile.close();
	}
	else{
		cout << "Unable to open dataFile" <<endl;
		errorOnBuild=true;
	}
}
/*
 * sets values to "attributes"
 * file's i-th line needs to be: <attrName>: <val1>,<val2>,...,<lastVal>
 * a continuous attribute's line has to be: <attrName>:
 * last line needs to be: class: <val1>,<val2>,...,<lastVal>
 */
void RandomForest::setAttrs(const std::string& attrFile){
	this->attributes->clear();
	bool firstWord=true;
	vector<DataVar*> *dataLn;
	string line,word;
	DataVar* token;
	ifstream myfile(attrFile.c_str(),ifstream::in);
	if(myfile.is_open()){
		while (! myfile.eof() ){
			dataLn=new vector<DataVar*>();
			getline(myfile,line);
			firstWord=true;
			while(line.length()>0){
				if(line.at(0)==' '){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				if(firstWord){
					word=line.substr(0,line.find(':',0));
					line=line.substr(line.find(':',0)+1);
					firstWord=false;
				}else{
					word=line.substr(0,line.find(',',0));
					if(word.length()==line.length()){
						line="";
					}else{
						line=line.substr(line.find(',',0)+1);
					}
				}
				while(word[word.length()-1]==' ')
					word=word.substr(0,word.length()-1);
				token=new DataVar(STRING,word);//tokenizeString(word);
				(*dataLn).push_back(token);
			}
			attributes->push_back(*dataLn);
		}
		myfile.close();
	}else{
		cout<< "can't open attrFile. aborting"<< endl;
		errorOnBuild=true;
	}
}

string RandomForest::toString() const{
	ostringstream ans(ostringstream::out);
	ans << "random forest print-out:\n";
	for(unsigned int j=0; j< size; j++){
		ans << "(" << j << ") " << this->forest[j]->toString();
	}
	return ans.str();
}

vector<DataVar*>& RandomForest::setToEval(vector<DataVar*>* evalRec,unsigned int treeInd) const{
	vector<DataVar*>* ans=new vector<DataVar*>();
	const vector<vector<DataVar*> > treeAtt=forest[treeInd]->getattributes();
	for(unsigned int i=0; i<treeAtt.size()-1; i++){
		bool found=false;
		for(unsigned int j=0; j< this->attributes->size()-1 && !found; j++){
			if(treeAtt.at(i).at(0) == this->attributes->at(j).at(0)){
				found=true;
				ans->push_back(evalRec->at(j));
			}
		}
	}
	return *ans;
}

string RandomForest::eval(vector< vector<DataVar*> > *evaldata) const{
	ostringstream ans(ostringstream::out);
	for(unsigned int i=0; i<evaldata->size(); i++){//for each record:
		ans << "line " << i << ": " ;
		float* classScore=new float[getNclasses()];
		for(int j=0; j<getNclasses(); j++)
			classScore[j]=0;
		for(unsigned int j=0; j < size; j++){//for each tree: add it's eval data
			int classification;
			float nodepercentage,nodeErr;
			vector<DataVar*> toEval=setToEval(&(evaldata->at(i)),j); //sets toEval for tree i.
			
			forest[j]->getEvalData(&toEval,&classification, &nodepercentage, &nodeErr);
			if(nodepercentage >= minPercentage){//otherwise ignore this decision...
				classScore[classification-1]+= (1-nodeErr); //can add to classScore[curr_classification] up to 1, according to tree's certainty.
			}
		}
		float maxVal=0;
		int decision=0;
		float pct=0;
		for(int j=0; j<getNclasses(); j++){
			if(classScore[j] > maxVal){
				decision=j;
				maxVal=classScore[j];
			}
			pct+=classScore[j]; //this first sums all scores.
		}
		pct= classScore[decision] / pct; // divide decision by total scores to get certainty percentage.
		ans << this->attributes->back().at(decision+1)->getStrVal() << " with certainty of " << pct << endl;
		delete[] classScore;
	}
	return ans.str();
}

string RandomForest::eval(const std::string& dataFile) const{
	ostringstream ans(ostringstream::out);
	vector< vector<DataVar*> > evaldata;
	unsigned int ind=0,i=0; //this one helps determine which line gets token.
	vector<DataVar*> *dataLn;
	string line,word;
	DataVar* token;
	ifstream myfile(dataFile.c_str(),ifstream::in);
	if (myfile.is_open()){
		while (! myfile.eof() ){
			getline (myfile,line);
			ind=0;
			dataLn= new vector<DataVar*>();
			while(line.length()>0 && dataLn->size()<attributes->size()-1 ){
				if(line.at(0)==' ' || line.at(0)=='\t'){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				word=line.substr(0,line.find(',',0));
				if(word.length()==line.length()){
					line="";
					if(ind < attributes->size()-2){
						return "missing data in record. aborting.\n";
					}
				}else{
					line=line.substr(line.find(',',0)+1);
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\t')
					word=word.substr(0,word.length()-1);

				if(attributes->at(ind).size()==1)//continuous variable
					token=new DataVar(NUMERIC,word);//tokenize(word);
				else{
					for(i=1; i<attributes->at(ind).size();i++)
						if(strcmp(word.c_str(),
								attributes->at(ind).at(i)->getStrVal().c_str())==0){
							token=attributes->at(ind).at(i);
							break; //for
						}
					if(i >= attributes->at(ind).size()){
						cout<< "invalid value on " << word.c_str() <<" aborting."<< endl;
						return "";
					}
				}//token is now the next data element to add
				if(ind==attributes->size()-1)
					break; //hence get next line
				dataLn->push_back(token);
				ind++;
			}
			if(dataLn->size()>0)
				evaldata.push_back(*dataLn);
			else
				dataLn->~vector();
		}
		myfile.close();
	}
	else return "Unable to open dataFile\n";
	return eval(&evaldata);
}
