/*
 * Excptn.cpp
 *
 *  Created on: Mar 3, 2010
 *      Author: operry5
 */

#include <exception>
#include <string>
#include "../h/Excptn.h"
using namespace std;

const char* Excptn::what() const throw()
  {
    return &_msg[0]; //a stupid type conversion
  }
Excptn::Excptn(const string msg):_msg(msg){}
Excptn::~Excptn(){}//does nothing? how about _msg's memory??
