/*
 * main.cpp
 *
 *  Created on: Nov 24, 2009
 *      Author: operry5
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include "../h/DecisionTree.h"
#include "../h/RandomForest.h"
#include "../h/EvoForest.h"
#include "../h/DataVar.h"
#include <string>
#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include <cstring>
using namespace std;

static enum{
	cart,
	Randomforest,
	Evolution
}currMode=cart;

vector< vector<DataVar*> > *attributes=NULL; //data-table headers and possible values
vector< vector<DataVar*> > *data=NULL; //the actual numbers (the data table)
DecisionTree* tree=NULL;
RandomForest* forest=NULL;
EvoForest* evoForest=NULL;
bool errOnBuild=false;

/*
 * sets values to "attributes"
 * file's i-th line needs to be: <attrName>: <val1>,<val2>,...,<lastVal>
 * a continuous attribute's line has to be: <attrName>:
 * last line needs to be: class: <val1>,<val2>,...,<lastVal>
 */
void setAttrs(const std::string& attrFile){
	if(attributes==NULL)
		attributes=new vector<vector<DataVar*> >();
	attributes->clear();
	bool firstWord=true;
	vector<DataVar*> dataLn;
	string line,word;
	DataVar* token;
	ifstream myfile(attrFile.c_str(),ifstream::in);
	if(myfile.is_open()){
		while (! myfile.eof() ){
			dataLn.clear();//=new vector<DataVar*>();
			getline(myfile,line);
			firstWord=true;
			while(line.length()>0){
				if(line.at(0)==' '){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				if(firstWord){
					word=line.substr(0,line.find(':',0));
					line=line.substr(line.find(':',0)+1);
					firstWord=false;
				}else{
					word=line.substr(0,line.find(',',0));
					if(word.length()==line.length()){
						line="";
					}else{
						line=line.substr(line.find(',',0)+1);
					}
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\r')
					word=word.substr(0,word.length()-1);
				if(line!="" || word!=""){
					token=new DataVar(STRING,word);
					dataLn.push_back(token);
				}
			}
			attributes->push_back(dataLn);
		}
		/*for(unsigned int i=0; i<attributes.size(); i++){
			printf("\nx%d: ",i);
			for(unsigned int j=0; j<attributes.at(i).size(); j++){
				if(attributes.at(i).at(j)->type==NUMERIC)
					printf("%f, ", *attributes.at(i).at(j)->dval.num);
				else{
					printf( (*(attributes.at(i).at(j)->dval.str)).c_str());
					printf(", ");
				}
			}
			printf("\n");
		}*/
	}else{
		cout <<"can't open attrFile"<< endl;
		errOnBuild=true;
		return;
	}
}
/*
 * this initializes "attributes", "classifications" and "data".
 * a record in dataFile must be of the form: <val1>,<val2>,...,<LastVal>,<class>
 */
void readData(const std::string& attrFile, const std::string& dataFile){
	errOnBuild=false;
	if(data==NULL)
		data=new vector<vector<DataVar*> >();
	data->clear();
	unsigned int ind=0,i=0; //this one helps determine which line gets token.
	vector<DataVar*> dataLn;
	string line,word;
	DataVar* token;
	setAttrs(attrFile);
	if(errOnBuild)
		return;
	int lineInd=0; //just for error notifications.
	ifstream myfile(dataFile.c_str(),ifstream::in);
	if (myfile.is_open()){
		while (! myfile.eof() ){
			getline (myfile,line);
			lineInd++;
			ind=0;
			dataLn.clear();//= new vector<DataVar*>();
			while(line.length()>0){
				if(line.at(0)==' ' || line.at(0)=='\t'){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				word=line.substr(0,line.find(',',0));
				if(word.length()==line.length()){
					line="";
					if(ind < attributes->size()-1){
						cout << "missing data in record (line:" << lineInd << "). aborting."<< endl;
						errOnBuild=true;
						return;
					}
				}else{
					line=line.substr(line.find(',',0)+1);
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\t' || word[word.length()-1]=='\r')
					word=word.substr(0,word.length()-1);

				if(attributes->at(ind).size()==1)//continuous variable
					token= new DataVar(NUMERIC,word);//=tokenize(word);
				else{
					for(i=1; i<attributes->at(ind).size();i++)
						if(strcmp(word.c_str(),
								attributes->at(ind).at(i)->getStrVal().c_str())==0){
							token=new DataVar(STRING,word); 
							break; //for
						}
					if(i >= attributes->at(ind).size()){
						cout<< "invalid value on " << word.c_str() << ". aborting." <<endl;
						errOnBuild=true;
						return;
					}
				}//token is now the next data element to add
				if(ind==attributes->size()){
					cout<< "too many values in record (line:" << lineInd << "). aborting" << endl;
					errOnBuild=true;
					return;
				}
				dataLn.push_back(token);
				ind++;
			}
			if(dataLn.size()>0)
				data->push_back(dataLn);
		}
		myfile.close();
		/*
		//DEBUG PRINTING
		cout<< "********************************************" << endl;
		cout<< "printing data:" << endl;
		for(i=0; i<data.size(); i++){
			cout << "line" << i << ": ";
			for(unsigned int j=0; j<data.at(i).size(); j++){
				if(data.at(i).at(j)->type==NUMERIC)
					cout <<  *(data.at(i).at(j)->dval.num) << " ";
				else
					cout <<  *(data.at(i).at(j)->dval.str) << " ";
			}
			//cout << *(classifications.at(i)->dval.str);
			cout << endl;
		}
		cout << "lines read:" << data.size() << "\n";*/
	}
	else{
		cout <<"Unable to open dataFile"<< endl;
		errOnBuild=true;
	}
}


void eval(){ //calls proper eval according to 'currMode'
	if(data==NULL || errOnBuild){
		cout<< "no training set available. run \"init\" first" << endl;
		return;
	}
	if(currMode==Randomforest && forest==NULL){
		cout<< "no forest available. run \"makeForest\" first" << endl;
		return;
	}
	if(currMode==Evolution && evoForest==NULL){
		cout<< "no evoForest available. run \"makeEvoForest\" first" << endl;
		return;
	}
	cout << "insert eval data from file? [y/n]" << endl;
	string str;
	getline(cin,str);
	if(strcmp(str.c_str(),"y")==0){
		cout << "type in file name>";
		getline(cin,str);
		cout << "log resaults into file?[y/n]";
		string log;
		getline(cin,log);
		ofstream outFile;
		if(strcmp(log.c_str(),"y")==0){
			bool ok=false;
			cout<< "type in a log file-name for writing>";
			while(!ok){
				getline(cin,log);
				outFile.open(log.c_str(),ofstream::out|ofstream::trunc);
				if(!outFile.is_open()){
					cout<< "can't open this file. specify another>";
				}else
					ok=true;
			}
		}
		if(currMode==cart){
			if(tree==NULL)
				tree=new DecisionTree(attributes,data);
			if(outFile!=NULL && outFile.is_open())
				outFile<< tree->eval(str);
			else
				cout << tree->eval(str);
		}else if(currMode==Randomforest){
			if(outFile!=NULL && outFile.is_open())
				outFile<< forest->eval(str);
			else
				cout << forest->eval(str);
		}else{ /*Evolution*/
			if(outFile!=NULL && outFile.is_open())
				outFile<< evoForest->eval(str);
			else
				cout << evoForest->eval(str);
		}
	}else{ //read from console
		vector< vector<DataVar*> > evaldata;
		vector<DataVar*> *dataLn;
		int i=1;
		cout << "inserting from console:" << endl;
		for(;;){
			cout << "record " << i << ":";
			dataLn=new vector<DataVar*>();
			for(unsigned int j=0; j<attributes->size()-1; j++){//get record
				cout << attributes->at(j).at(0)->getStrVal()/*dval.str*/ << ":";
				string in;
				getline(cin,in);
				float* input=new float((float)atof(in.c_str()));
				dataLn->push_back(new DataVar(*input)/*makeDataVar(input)*/);
			}
			evaldata.push_back(*dataLn);
			cout << "insert more? [y/n]:";
			string cont;
			getline(cin,cont);
			if(strcmp(cont.c_str(),"n")==0)
				break;
		}
		if(currMode==cart){
			if(tree==NULL)
				tree=new DecisionTree(attributes,data);
			cout << tree->eval(&evaldata);
		}else if(currMode==Randomforest)
			cout << forest->eval(&evaldata);
		else /*Evolution*/
			cout << evoForest->eval(&evaldata);
	}
}

void printmenu(){
	cout << "possible commands:" << endl
			<< "  (-) init <attr_filename> <data_filename> --> insert new learning set" << endl
			<< "  (-) formatInfo --> a short explanaition about input file's format" <<endl
			<< "  (-) menu --> to re-print this menu" << endl
			<< "  (-) exit --> quit " << endl
			<< "\n CART TREE OPTIONS: "<<endl
			<< "  (-) viewTree --> show CART tree" << endl
			<< "  (-) nodeerr <all / node_num> -->print error data for a node /all nodes" << endl
			<< "  (-) classcount <all / node_num> -->print classifications on node / all nodes" << endl
			<< "  (-) treeEval --> ask for tree classifications" <<endl
			<< "\n RANDOM FOREST OPTIONS: "<<endl
			<< "  (-) makeForest <amountOfTrees> <attrsPerTree> --> creates a random forest" <<endl
			<< "  (-) viewForest --> forest printout (not reccomended for large forests)" << endl
			<< "  (-) forestEval --> ask for forest classifications" << endl
			<< "\n EVOLUTION FOREST OPTIONS: "<<endl
			<< "  (-) makeEvoForest <generations> <amountOfTrees> <attrsPerTree> --> creates an EvoForest" <<endl
			<< "  (-) viewEvoForest --> forest printout (not reccomended for large forests)" << endl
			<< "  (-) evoForestEval --> ask for forest classifications" << endl;
			
}

void clearAll(){
	if(tree!=NULL){
		delete tree;
		tree=NULL;
	}
	if(forest!=NULL){
		delete forest;
		forest=NULL;
	}
	if(evoForest!=NULL){
		delete evoForest;
		evoForest=NULL;
	}
	if(attributes!=NULL){
		for(unsigned int i=0; i<attributes->size(); i++){
			for(unsigned int j=0; j<attributes->at(i).size(); j++){
				delete(attributes->at(i).at(j));
			}
		}
		delete attributes;
		attributes=NULL;
	}
	if(data!=NULL){
		for(unsigned int i=0; i<data->size(); i++){
			for(unsigned int j=0; j<data->at(i).size(); j++){
				delete(data->at(i).at(j));
			}
		}
		delete data;
		data=NULL;
	}
	errOnBuild=false;
}

void ui(){
	printmenu();
	for(;;){
		cout << ">";
		string str;
		getline(cin,str);
		if(strncmp(str.c_str(),"init ",5)==0){
			string prm1,prm2;
			prm1=str.substr(5);
			const char *ptr=strchr(prm1.c_str(),' ');
			if(ptr==NULL){
				cout<< "error. file names unspecified." <<endl;
				continue;
			}
			const char *ptr2=prm1.c_str();
			prm2=prm1.substr(ptr-ptr2+1);
			prm1=prm1.substr(0,ptr-ptr2);
			//cout << "prm1: " << prm1 << "prm2: " <<prm2  <<"kkk"<< endl;
			if(tree!=NULL){
				delete tree;
				tree=NULL;
			}
			if(forest!=NULL){
				delete forest;
				forest=NULL;
			}
			if(prm1=="" || prm2==""){
				cout<< "error. file names unspecified."<<endl;
				continue;
			}
			readData(prm1,prm2);
			//actual action
		}else if(strcmp(str.c_str(),"formatInfo")==0){
			cout<< "file formatts:" << endl << "\nattribute file: \n" <<
				"  each line must be of the form: <attr-name>: <val1> .. <valk>, where <val>s are possible values" << endl
				<< "  (no <val>s if continuous variable)" << endl 
				<< "  last line must be of the form: class: <val1> ... <valk>, where <val>s are possible classifiations"<<
				endl << "\ndata file: \n  each line must be of the form: <val1>, ..,<valk>,<classification>"<<endl
				<< "  node that the amount of values must be the same as the amount of attributes in the first file"<<endl
				<< "  also, the classification must be one of the values stated in that file." <<endl
				<< "**everything is case-sensitive! \n***this program only supports continuous variables at the moment"
				<<endl;
		}else if(strcmp(str.c_str(),"menu")==0){
			printmenu();
		}else if(strcmp(str.c_str(),"exit")==0){
			clearAll();
			break;
		}else if(strcmp(str.c_str(),"viewTree")==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no tree available. run \"init\" first" << endl;
				continue;
			}
			if(tree==NULL){
				tree=new DecisionTree(attributes,data);
			}
			cout<< tree->toString();
		}else if(strncmp(str.c_str(),"nodeerr ",8)==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no tree available. run \"init\" first" << endl;
				continue;
			}
			if(tree==NULL)
				tree=new DecisionTree(attributes,data);
			str=str.substr(8);
			if(strcmp(str.c_str(),"all")==0)
				cout << tree->getnodeerr(-1);
			else cout << tree->getnodeerr(atoi(str.c_str()));
		}else if(strncmp(str.c_str(),"classcount ",11)==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no tree available. run \"init\" first" << endl;
				continue;
			}
			if(tree==NULL)
				tree=new DecisionTree(attributes,data);
			str=str.substr(11);
			if(strcmp(str.c_str(),"all")==0)
				cout << tree->getclasscount(-1);
			else cout << tree->getclasscount(atoi(str.c_str()));
		}else if(strcmp(str.c_str(),"treeEval")==0){
			currMode=cart; //to tell eval what eval to run (we can switch that with a simple parameter pass...)
			eval();
		}else if(strncmp(str.c_str(),"makeForest ",11)==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no training set available. run \"init\" first" << endl;
				continue;
			}
			int attrs,amount;
			string prm;
			prm=str.substr(11);
			if( (amount=atoi(prm.c_str()))<=0){
				cout << "error. first param should be amount of trees in forest >0. ignoring command" <<endl;
				continue;
			}
			int tmp=(int)ceil(log10((float)amount))+1;
			prm=prm.substr(tmp);
			if( (attrs=atoi(prm.c_str()))<=0){
				cout << "error. second param should be amount of attributes per tree >0. ignoring command" <<endl;
				continue;
			}
			if(attrs >= (int)attributes->size()){
				cout << "error. second param should be amount of attributes per tree."<<endl
					<<"total attributes in file: "<< attributes->size()-1 <<". ignoring command." <<endl;
				continue;
			}
			if(forest!=NULL)
				delete forest;
			forest=new RandomForest(amount,attrs,attributes,data);
		}else if(strcmp(str.c_str(),"viewForest")==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no training set available. run \"init\" first" << endl;
				continue;
			}
			if(forest==NULL){
				cout<< "no forest available. run \"makeForest\" first" << endl;
				continue;
			}
			cout<< forest->toString();
		}else if(strcmp(str.c_str(),"forestEval")==0){
			currMode=Randomforest; //to tell eval what eval to run (we can switch that with a simple parameter pass...)
			eval();
		}else if(strncmp(str.c_str(),"makeEvoForest ",14)==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no training set available. run \"init\" first" << endl;
				continue;
			}
			int attrs,amount,gens;
			string prm;
			prm=str.substr(14);
			if( (gens=atoi(prm.c_str()))<=0){
				cout << "error. first param should be amount of generations >0. ignoring command" <<endl;
				continue;
			}
			int tmp=(int)ceil(log10((float)gens))+1;
			prm=prm.substr(tmp);
			if( (amount=atoi(prm.c_str()))<=0){
				cout << "error. second param should be amount of trees in forest >0. ignoring command" <<endl;
				continue;
			}
			tmp=(int)ceil(log10((float)amount))+1;
			prm=prm.substr(tmp);
			if( (attrs=atoi(prm.c_str()))<=0){
				cout << "error. third param should be amount of attributes per tree >0. ignoring command" <<endl;
				continue;
			}
			if(attrs >= (int)attributes->size()){
				cout << "error. third param should be amount of attributes per tree."<<endl
					<<"total attributes in file: "<< attributes->size()-1 <<". ignoring command." <<endl;
				continue;
			}
			if(evoForest!=NULL)
				delete evoForest;
			evoForest=new EvoForest(gens,amount,attrs,attributes,data);
		}else if(strcmp(str.c_str(),"viewEvoForest")==0){
			if(attributes==NULL || errOnBuild){
				cout<< "no training set available. run \"init\" first" << endl;
				continue;
			}
			if(evoForest==NULL){
				cout<< "no evoForest available. run \"makeEvoForest\" first" << endl;
				continue;
			}
			cout<< evoForest->toString();
		}else if(strcmp(str.c_str(),"evoForestEval")==0){
			currMode=Evolution; //to tell eval what eval to run (we can switch that with a simple parameter pass...)
			eval();
		}else 
			cout<< "unknown command. ignoring. \n *note that commands are cAsE SeNsitive" << endl;
	}
}

int main(int , char* []){
	try{
		ui();
	//	DecisionTree dt(argv[1],argv[2]);
	//	cout << dt.toString().str();
	}catch(const char* m){
		printf("exception: %s",m);
	}

	return 0;
}
