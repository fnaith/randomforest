/*
 * DecisionTree.cpp
 *
 *  Created on: Feb 28, 2010
 *      Author: operry5
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <math.h>
#include <limits>
#include <malloc.h>
#include "../h/DecisionTree.h"
#include "../h/DataVar.h"

#include <cstring>


using namespace std;

DecisionTree::DecisionTree():minleaf(1),minparent(10),nclasses(0),nodenumber(0),m(0),parent(NULL),
							children(NULL),nodesize(NULL),cutvar(NULL),classcount(NULL),yfitnode(NULL),
							cutpoint(NULL),nodeprob(NULL),resuberr(NULL),classprob(NULL),impurity(NULL),
							attributes(new vector<vector<DataVar*> >()),data(new vector<vector<DataVar*> >()),
							constructedWithPtrs(false),fitness(-1){
	//one must invoke readData(...) & maketree() after this one...
}

DecisionTree::DecisionTree(const std::string& attrFile,const std::string& dataFile):
		minleaf(1),minparent(10),nclasses(0),nodenumber(0),m(0),parent(NULL),
		children(NULL),nodesize(NULL),cutvar(NULL),classcount(NULL),yfitnode(NULL),
		cutpoint(NULL),nodeprob(NULL),resuberr(NULL),classprob(NULL),impurity(NULL),
		attributes(new vector<vector<DataVar*> >()),data(new vector<vector<DataVar*> >()),
		errOnBuild(false),constructedWithPtrs(false),fitness(-1){
	readData(attrFile, dataFile);
	if(errOnBuild)
		return;
	makeTree();
}

DecisionTree::DecisionTree(vector< vector<DataVar*> > *attrs, vector< vector<DataVar*> > *data):
		minleaf(1),minparent(10),nclasses(0),nodenumber(0),m(0),parent(NULL),
		children(NULL),nodesize(NULL),cutvar(NULL),classcount(NULL),yfitnode(NULL),
		cutpoint(NULL),nodeprob(NULL),resuberr(NULL),classprob(NULL),impurity(NULL),attributes(attrs),data(data),
		errOnBuild(false),constructedWithPtrs(true),fitness(-1){
	makeTree();
}

DecisionTree::DecisionTree(const DecisionTree& other):attributes(),data(){
//	cout<< "DecisionTree use of copy constructor" << endl;

	this->minleaf=other.minleaf;
	this->minparent=other.minparent;
	this->nclasses=other.nclasses;
	this->nodenumber=other.nodenumber;
	this->m=other.m;

	this->parent=new int[m];
	this->children=new int*[2];
	this->children[0]=new int[m];
	this->children[1]=new int[m];
	this->nodesize=new int[m];
	this->cutvar=new int[m];
	this->classcount=new int*[m];
	this->yfitnode=new int[m];
	
	this->cutpoint=new float[m];
	this->nodeprob=new float[m];
	this->resuberr=new float[m];
	this->classprob=new float*[m];
	this->impurity=new float[m];

	for(int i=0; i<m; i++){
		this->parent[i]=other.parent[i];
		this->children[0][i]=other.children[0][i];
		this->children[1][i]=other.children[1][i];
		this->nodesize[i]=other.nodesize[i];
		this->cutvar[i]=other.cutvar[i];
		this->yfitnode[i]=other.yfitnode[i];
	
		this->cutpoint[i]=other.cutpoint[i];
		this->nodeprob[i]=other.nodeprob[i];
		this->resuberr[i]=other.resuberr[i];
		this->impurity[i]=other.impurity[i];

		this->classcount[i]=new int[nclasses];
		this->classprob[i]=new float[nclasses];
		for(int j=0; j<nclasses; j++){
			this->classcount[i][j]=other.classcount[i][j];
			this->classprob[i][j]=other.classprob[i][j];
		}
	}
	
	this->attributes=new vector< vector<DataVar*> >();
	for(unsigned int i=0; i<other.attributes->size(); i++)
		this->attributes->push_back(other.attributes->at(i));
	this->data=new vector< vector<DataVar*> >();
	this->data->resize(other.data->size());
	for(unsigned int i=0; i<other.data->size(); i++)
		for(unsigned int j=0; j<other.data->at(i).size(); j++)
			this->data->at(i).push_back(other.data->at(i).at(j));
	
	this->errOnBuild=other.errOnBuild;
	this->constructedWithPtrs=other.constructedWithPtrs;
	this->fitness=other.fitness;
}

DecisionTree::~DecisionTree(){//WHO NEEDS TO "DEEP-DELETE" stuff and who doesn't?
	if(!constructedWithPtrs){
		for(unsigned int i=0; i<attributes->size(); i++){
			//attributes->at(i).clear();
			for(unsigned int j=0; j<attributes->at(i).size(); j++){
				delete(attributes->at(i).at(j));
			}
		}
		delete attributes;
		for(unsigned int i=0; i<data->size(); i++){
			//data->at(i).clear();
			for(unsigned int j=0; j<data->at(i).size(); j++){
				delete(data->at(i).at(j));
			}
		}
		delete data;
	}
	//delete attributes;
	//delete data;
	delete[] parent;
	delete[] children[0];
	delete[] children[1];
	delete[] children;
	delete[] nodesize;
	delete[] cutvar;
	for(int i=0; i<m; i++){
		delete[] classprob[i];
		delete[] classcount[i];
	}
	delete[] classprob;
	delete[] classcount;
	delete[] yfitnode;

	delete[] cutpoint;
	delete[] nodeprob;
	delete[] resuberr;
	delete[] impurity;
}

void DecisionTree::clearAtrNdata(){ //memory leak?
	delete attributes;
	delete data;
}

DecisionTree& DecisionTree::operator=(const DecisionTree& t){
	cout<< "DecisionTree use of operator=" <<endl;
	if(this==&t)
		return *this;
	if(this!=NULL){
		if(!constructedWithPtrs){
			for(unsigned int i=0; i<attributes->size(); i++){
				for(unsigned int j=0; j<attributes->at(i).size(); j++){
					delete(attributes->at(i).at(j));
				}
			}
			for(unsigned int i=0; i<data->size(); i++){
				for(unsigned int j=0; j<data->at(i).size(); j++){
					delete(data->at(i).at(j));
				}
			}
		}
		delete attributes;
		delete data;
		delete[] parent;
		delete[] children[0];
		delete[] children[1];
		delete[] children;
		delete[] nodesize;
		delete[] cutvar;
		for(int i=0; i<m; i++){
			delete[] classprob[i];
			delete[] classcount[i];
		}
		delete[] classprob;
		delete[] classcount;
		delete[] yfitnode;
		delete[] cutpoint;
		delete[] nodeprob;
		delete[] resuberr;
		delete[] impurity;
	}
	this->minleaf=t.minleaf;
	this->minparent=t.minparent;
	this->nclasses=t.nclasses;
	this->nodenumber=t.nodenumber;
	this->m=t.m;

	this->parent=new int[m];
	this->children=new int*[2];
	this->children[0]=new int[m];
	this->children[1]=new int[m];
	this->nodesize=new int[m];
	this->cutvar=new int[m];
	this->classcount=new int*[m];
	this->yfitnode=new int[m];
	
	this->cutpoint=new float[m];
	this->nodeprob=new float[m];
	this->resuberr=new float[m];
	this->classprob=new float*[m];
	this->impurity=new float[m];

	for(int i=0; i<m; i++){
		this->parent[i]=t.parent[i];
		this->children[0][i]=t.children[0][i];
		this->children[1][i]=t.children[1][i];
		this->nodesize[i]=t.nodesize[i];
		this->cutvar[i]=t.cutvar[i];
		this->yfitnode[i]=t.yfitnode[i];
	
		this->cutpoint[i]=t.cutpoint[i];
		this->nodeprob[i]=t.nodeprob[i];
		this->resuberr[i]=t.resuberr[i];
		this->impurity[i]=t.impurity[i];

		this->classcount[i]=new int[nclasses];
		this->classprob[i]=new float[nclasses];
		for(int j=0; j<nclasses; j++){
			this->classcount[i][j]=t.classcount[i][j];
			this->classprob[i][j]=t.classprob[i][j];
		}
	}
	
	this->attributes=new vector< vector<DataVar*> >();
	for(unsigned int i=0; i<t.attributes->size(); i++)
		this->attributes->push_back(t.attributes->at(i));
	this->data=new vector< vector<DataVar*> >();
	this->data->resize(t.data->size());
	for(unsigned int i=0; i<t.data->size(); i++)
		for(unsigned int j=0; j<t.data->at(i).size(); j++)
			this->data->at(i).push_back(t.data->at(i).at(j));
	
	this->errOnBuild=t.errOnBuild;
	this->constructedWithPtrs=t.constructedWithPtrs;
	this->fitness=t.fitness;

	return *this;
}


/*
 * this initializes "attributes", "classifications" and "data".
 * a record in dataFile must be of the form: <val1>,<val2>,...,<LastVal>,<class>
 */
void DecisionTree::readData(const std::string& attrFile, const std::string& dataFile){
	this->data->clear();
	unsigned int ind=0,i=0; //this one helps determine which line gets token.
	vector<DataVar*> dataLn;
	string line,word;
	DataVar* token;
	setAttrs(attrFile);
	if(errOnBuild)
		return;
	int lineInd=0; //just for error notifications.
	ifstream myfile(dataFile.c_str(),ifstream::in);
	if (myfile.is_open()){
		while (! myfile.eof() ){
			getline (myfile,line);
			lineInd++;
			ind=0;
			dataLn.clear();//= new vector<DataVar*>();
			while(line.length()>0){
				if(line.at(0)==' ' || line.at(0)=='\t'){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				word=line.substr(0,line.find(',',0));
				if(word.length()==line.length()){
					line="";
					if(ind < attributes->size()-1){
						cout << "missing data in record (line:" << lineInd << "). aborting."<< endl;
						errOnBuild=true;
						return;
					}
				}else{
					line=line.substr(line.find(',',0)+1);
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\t' || word[word.length()-1]=='\r')
					word=word.substr(0,word.length()-1);

				//printf("attrs->at(%d).size()=%d\n",ind,attributes->at(ind).size());
				if(attributes->at(ind).size()==1)//continuous variable
					token= new DataVar(NUMERIC,word);//=tokenize(word);
				else{
					for(i=1; i<attributes->at(ind).size();i++)
						if(strcmp(word.c_str(),
								attributes->at(ind).at(i)->getStrVal()/*dval.str))*/.c_str())==0){
							token=new DataVar(STRING,word); //TODO change 11: attributes->at(ind).at(i);
							break; //for
						}
					if(i >= attributes->at(ind).size()){
						cout<< "invalid value on " << word.c_str() << ". aborting." <<endl;
						errOnBuild=true;
						return;
					}
				}//token is now the next data element to add
				if(ind==attributes->size()){
					cout<< "too many values in record (line:" << lineInd << "). aborting" << endl;
					errOnBuild=true;
					return;
				}
				dataLn.push_back(token);
				ind++;
			}
			if(dataLn.size()>0)
				data->push_back(dataLn);
		}
		myfile.close();
		/*
		//DEBUG PRINTING
		cout<< "********************************************" << endl;
		cout<< "printing data:" << endl;
		for(i=0; i<data.size(); i++){
			cout << "line" << i << ": ";
			for(unsigned int j=0; j<data.at(i).size(); j++){
				if(data.at(i).at(j)->type==NUMERIC)
					cout <<  *(data.at(i).at(j)->dval.num) << " ";
				else
					cout <<  *(data.at(i).at(j)->dval.str) << " ";
			}
			//cout << *(classifications.at(i)->dval.str);
			cout << endl;
		}
		cout << "lines read:" << data.size() << "\n";*/
	}
	else{
		cout <<"Unable to open dataFile"<< endl;
		errOnBuild=true;
	}
}
/*
 * sets values to "attributes"
 * file's i-th line needs to be: <attrName>: <val1>,<val2>,...,<lastVal>
 * a continuous attribute's line has to be: <attrName>:
 * last line needs to be: class: <val1>,<val2>,...,<lastVal>
 */
void DecisionTree::setAttrs(const std::string& attrFile){
	this->attributes->clear();
	bool firstWord=true;
	vector<DataVar*> dataLn;
	string line,word;
	DataVar* token;
	ifstream myfile(attrFile.c_str(),ifstream::in);
	if(myfile.is_open()){
		while (! myfile.eof() ){
			dataLn.clear();//=new vector<DataVar*>();
			getline(myfile,line);
			firstWord=true;
			while(line.length()>0){
				if(line.at(0)==' '){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				if(firstWord){
					word=line.substr(0,line.find(':',0));
					line=line.substr(line.find(':',0)+1);
					firstWord=false;
				}else{
					word=line.substr(0,line.find(',',0));
					if(word.length()==line.length()){
						line="";
					}else{
						line=line.substr(line.find(',',0)+1);
					}
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\r')
					word=word.substr(0,word.length()-1);
				if(line!="" || word!=""){
					token=new DataVar(STRING,word);//tokenizeString(word);
					dataLn.push_back(token);
				}
			}
			attributes->push_back(dataLn);
		}
		/*for(unsigned int i=0; i<attributes.size(); i++){
			printf("\nx%d: ",i);
			for(unsigned int j=0; j<attributes.at(i).size(); j++){
				if(attributes.at(i).at(j)->type==NUMERIC)
					printf("%f, ", *attributes.at(i).at(j)->dval.num);
				else{
					printf( (*(attributes.at(i).at(j)->dval.str)).c_str());
					printf(", ");
				}
			}
			printf("\n");
		}*/
	}else{
		cout <<"can't open attrFile"<< endl;
		errOnBuild=true;
		return;
	}
}

/*
 * invoked after reading from file, this method builds the actual tree.
 */
void DecisionTree::makeTree(){
	/*M = 2*ceil(N/minleaf)-1;% number of tree nodes for space reservation
	nodenumber = zeros(M,1);
	parent = zeros(M,1);
	yfitnode = zeros(M,1);
	cutvar = zeros(M,1);
	cutpoint = cell(M,1);
	children = zeros(M,2);
	nodeprob = zeros(M,1);
	resuberr = zeros(M,1);
	nodesize = zeros(M,1);
	if doclass
	   classprob = zeros(M,nclasses);
	   classcount = zeros(M,nclasses);
	   if isimpurity==1
	       impurity = zeros(M,1);
	   end
	end
	iscat = zeros(nvars,1); iscat(categ) = 1; %DO WE NEED ISCAT*/

	/*init*/
	m =(int) (2*ceil((float)(data->size()/minleaf))-1);//number of tree nodes for space reservation
	nodenumber = 0;
	parent = new int[m];
	yfitnode = new int[m];
	cutvar = new int[m];
	cutpoint = new float[m];
	children = new int*[2];
	children[0] = new int[m];
	children[1] = new int[m];
	nodeprob = new float[m];
	resuberr = new float[m];
	nodesize = new int[m];
	for (int i=0; i<m ; i++){
			parent[i] = -1;
			yfitnode[i] = -1;
			cutvar[i] = -1;
			cutpoint[i] = -1;
			children[0][i] = -1;
			children[1][i] = -1;
			nodeprob[i] = 0;
			resuberr[i] = 0;
			nodesize[i] = 0;
		}
	nclasses = (int)attributes->at(attributes->size()-1).size()-1; //the last -1 is since the first element is the word "class"
	classprob = new float*[m]; //number of classifications
	classcount = new int*[m];
	for(int i=0 ; i<m ;i++){
		classcount[i] = new int[nclasses];
		classprob[i] = new float[nclasses];
	}
	impurity = new float[m];
	//iscat = IS ALWAYS 0
	/*
	 *  nodenumber(1) = 1;
		assignednode = cell(M,1);% list of instances assigned to this node
		assignednode{1} = 1:N;
		nextunusednode = 2;
	 *
	 */
	//nodenumber[0] = 1;
	nodenumber=1;
	vector< vector<int> > assignednode;//[data.at(1).size()];
	vector<int> tmp;//=new vector<int>();
	for(unsigned int i=0 ; i< data->size() ; i++)
		tmp.push_back(i);
	assignednode.push_back(tmp);
	/*for(int i=1; i<m ;i++)
		assignednode.push_back(*(new vector<int>()));*/
	assignednode.resize(m);
	int nextunusednode = 1;
	/*
	 * if doclass
   % Get default or specified prior class probabilities
   Prior = Prior(:)';
   haveprior = true;
   if isempty(Prior)
      Prior = Nj / N;
      haveprior = false;
   elseif isequal(Prior,'equal')
      Prior = ones(1,nclasses) / nclasses;
   elseif isstruct(Prior)
      if ~isfield(Prior,'group') || ~isfield(Prior,'prob')
         error('stats:treefit:BadPrior',...
              'Missing field in structure value for ''priorprob'' parameter.');
      end
      idx = getclassindex(cnames,Prior.group);
      if any(idx==0)
         j = find(idx==0);
         error('stats:treefit:BadPrior',...
               'Missing prior probability for group ''%s''.',cnames{j(1)});
      end
      Prior = Prior.prob(idx);
   end
   if length(Prior)~=nclasses || any(Prior<0) || sum(Prior)==0 ...
                              || ~isnumeric(Prior)
      error('stats:treefit:BadPrior',...
            'Value of ''priorprob'' parameter must be a vector of %d probabilities.',...
            nclasses);
   elseif all(Prior==0 | sum(C,1)==0)
      error('stats:treefit:BadPrior',...
            'The ''priorprob'' parameter assigns all probability to unobserved nclasses.');
   else
      Prior = Prior / sum(Prior);
   end
   % Get default or specified misclassification costs
   havecosts = true;
   if isempty(Cost)
      Cost = ones(nclasses) - eye(nclasses);
      havecosts = false;
   else
      if isstruct(Cost)
         if ~isfield(Cost,'group') || ~isfield(Cost,'cost')
            error('stats:treefit:BadCost',...
                  'Missing field in structure value for ''cost'' parameter.');
         end
         idx = getclassindex(cnames,Cost.group);
         if any(idx==0)
            j = find(idx==0);
            error('stats:treefit:BadCost',...
                  'Missing misclassification cost for group ''%s''.',...
                          cnames{j(1)});
         end
         Cost = Cost.cost(idx,idx);
      end
      if ~isequal(size(Cost),nclasses*ones(1,2))
         error('stats:treefit:BadCost',...
               'Misclassification cost matrix must be %d-by-%d.',...
                       nclasses,nclasses);
      elseif any(diag(Cost)~=0)
         error('stats:treefit:BadCost',...
            'Misclassification cost matrix must have zeros on the diagonal.');
      elseif any(Cost<0)
         error('stats:treefit:BadCost',...
            'Misclassification cost matrix must contain non-negative values.');
      end
   end
   % Adjust priors if required to take misclassification costs into account
   adjprior = Prior;
   if havecosts
      Cj = sum(Cost,2)';
      pc = Cj .* Prior;
      adjprior = pc / sum(pc);
   end
   pratio = adjprior ./ max(1,Nj);
	 */
	float* prior=new float[nclasses];
	bool** C=new bool*[data->size()];
	for(unsigned int i=0; i<data->size(); i++){
		C[i]=new bool[nclasses];	//TODO - is this true or false?
		for(int j=0; j<nclasses; j++)
			C[i][j]=false;
	}
	int* nj=new int[nclasses];
	for(int i=0; i<nclasses; i++){
		prior[i]=0;
		nj[i]=0;
	}
	for(unsigned int i=0;i<data->size();i++){
		DataVar* curr=data->at(i).at(data->at(i).size()-1);
		for(int j=1 ; j<= nclasses ; j++){
			//DataVar
			if(attributes->at(attributes->size()-1).at(j)->getType()==NUMERIC){
				if(attributes->at(attributes->size()-1).at(j)->getNumVal()==curr->getNumVal()){
					nj[j-1]++;
					C[i][j-1]=true;
				}else
					C[i][j-1]=false;
			}else if(strcmp(attributes->at(attributes->size()-1).at(j)->getStrVal().c_str(),
					curr->getStrVal().c_str())==0){
				nj[j-1]++;
				C[i][j-1]=true;
			} else
				C[i][j-1]=false;
		}
	}

	float sumprior=0;
	for(int i=0 ; i<nclasses ; i++){
		prior[i] = nj[i]/(float)data->size();
		sumprior += prior[i];
	}
	for(int i=0 ; i<nclasses ; i++){
		prior[i] = prior[i]/sumprior;
	}
	int** cost=new int*[nclasses];
	for(int i=0; i<nclasses ;i++){
		cost[i]=new int[nclasses];
		for(int j=0; j<nclasses ; j++){
			if(i!=j)
				cost[i][j]=1;
			else
				cost[i][j]=0;
		}
	}
	float* pratio=new float[nclasses];
	for(int i=0; i< nclasses ; i++)
		pratio[i] = prior[i] / max(1,nj[i]);

	//end of initializing


	/*
	% Keep processing nodes until done
	tnode = 1;
	while(tnode < nextunusednode)
	   % Record information about this node
	   noderows = assignednode{tnode};
	   Nnode = length(noderows);
	   Cnode = C(noderows,:);
	   if doclass
	      % Compute class probabilities and related statistics for this node
	      Njt = sum(Cnode,1);    % number in class j at node t
	      Pjandt = Prior .* Njt ./ max(1,Nj);   %max() to get 0/1 if Njt=Nj=0
	      Pjgivent = Pjandt / sum(Pjandt);
	      misclasscost = Pjgivent * Cost;
	      [mincost,nodeclass] = min(misclasscost);
	      yfitnode(tnode) = nodeclass;
	      Pt = sum(Pjandt);
	      nodeprob(tnode) = Pt;
	      classprob(tnode,:) = Pjgivent;
	      classcount(tnode,:) = Njt;
	      impure = sum(Pjgivent>0)>1;
	      if isimpurity==1
	          impurity(tnode) = feval(critfun,classprob(tnode,:));
	      end
	 */
	
	int tnode = 0;
	while(tnode < nextunusednode){ //MAIN WHILE
		int Nnode = (int)assignednode[tnode].size();
		bool** Cnode=new bool*[Nnode];	///Cnode = C[noderows]
		for(int i=0 ; i < Nnode ;i++){
			Cnode[i] = C[assignednode[tnode].at(i)] ;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		}
		int* Njt=new int[nclasses];
		for(int i=0; i<nclasses; i++)
			Njt[i]=0;
		for(int i=0 ; i < Nnode ; i++){
			for(int j=0 ; j < nclasses ; j++){
				if(Cnode[i][j] == true){
					Njt[j]++;
					break;
				}
			}
		}
		
		delete[] Cnode;
		float* Pjandt=new float[nclasses];
		for(int i=0; i<nclasses ; i++){
			Pjandt[i] = prior[i] * Njt[i] / max(1, nj[i]);
		}

		float* Pjgivent=new float[nclasses];
		float sumPjandt = 0;
		for(int i=0 ; i < nclasses ; i++){
			sumPjandt+=Pjandt[i];
		}
		for(int i=0 ; i<nclasses ; i++){
			Pjgivent[i] = Pjandt[i]/sumPjandt;
		}
		float* misclasscost=new float[nclasses];
		for(int i=0 ; i < nclasses ; i++){
			misclasscost[i]=0;
			for(int j=0 ; j < nclasses ; j++){
				misclasscost[i]+= Pjgivent[j] * cost[i][j];
			}
		}
		delete[] Pjandt;
		int nodeclass=0;
		float mincost=0;
		mincost = misclasscost[0];
		for(int i=1 ; i < nclasses ; i++){
			if(misclasscost[i]<mincost){
				mincost=misclasscost[i];
				nodeclass = i;
			}
		}
		
		delete[] misclasscost;
		yfitnode[tnode] = nodeclass;
		nodeprob[tnode] = sumPjandt;
		for(int i=0 ; i<nclasses ; i++){
			classprob[tnode][i] = Pjgivent[i];
			classcount[tnode][i] = Njt[i];
		}
		int sumOfPosPjgivent=0;
		for(int i=0; i<nclasses ; i++){
			if(Pjgivent[i]>0)
				sumOfPosPjgivent++;
		}
		bool impure = sumOfPosPjgivent>1;
		impurity[tnode] = gdi(classprob[tnode],nclasses);
		delete[] Pjgivent;
		delete[] Njt;
		/*
		 * bestcrit          = -Inf;
	   nodesize(tnode)   = Nnode;
	   resuberr(tnode)   = mincost;
	   cutvar(tnode)     = 0;
	   cutpoint{tnode}   = 0;
	   children(tnode,:) = 0;

	   % Consider splitting this node
	   if (Nnode>=minparent) && impure      % split only large impure nodes
		  Xnode = X(noderows,:);
		  bestvar = 0;
		  bestcut = 0;
		 */
		float bestcrit = - numeric_limits<float>::max(); //bestcrit now holds the smallest posible value for a "float"
		nodesize[tnode] = Nnode;
		resuberr[tnode] = mincost;
		cutvar[tnode] = -1;//0;
		//cutpoint[tnode][0] = 0;
		children[0][tnode] = 0;
		children[1][tnode] = 0;
		//Xnode is data.at(assignednode).at(tnode)
		if(Nnode>=minparent && impure){ //split only large impure nodes
			int bestvar = -1;
			float bestcut = 0;
			/*
			 * an optional var code - if we don't want to sample all variables
			 * % Reduce the number of predictor vars as specified by nvarstosample
      varmap = 1:nvars;
      if nusevars < nvars
          varmap = randsample(nvars,nusevars);
      end
			 */
			/*
	  % Find the best of all possible splits
      for ivar=1:nusevars
         % Index of variable to split on
         jvar = varmap(ivar);

         %00 Get rid of missing values and sort this variable
         idxnan = isnan(Xnode(:,jvar));
         idxnotnan = find(~idxnan);
         if isempty(idxnotnan)
             continue;
         end
         [x,idxsort] = sort(Xnode(idxnotnan,jvar));
         idx = idxnotnan(idxsort);
         c = Cnode(idx,:);
         if doclass
             Ccum = cumsum(c,1);
         else
             Ccum = (1:size(c,1))';
         end
			 */
			/*
			 * % Determine if there's anything to split along this variable
		maxeps = max(eps(x(1)), eps(x(end)));
    	if x(1)+maxeps > x(end)
			continue;
		end
	   % Accept only splits on rows with distinct values
         rows = find( x(1:end-1) + ...
             max([eps(x(1:end-1)) eps(x(2:end))],[],2) < x(2:end) );
         if isempty(rows)
            continue;
         end

         % Categorical variable?
         xcat = iscat(jvar);
			 */
			for(unsigned int i=0 ; i < data->at(0).size()-1 ; i++){ //big for
				int* idx=new int[Nnode];
				float* x=new float[Nnode];
				for(int j = 0 ;j <  Nnode ; j++){
					x[j] = data->at(assignednode.at(tnode).at(j)).at(i)->getNumVal();
					idx[j] = assignednode.at(tnode).at(j);
				}
				sort(x , idx , Nnode);
				bool** c=new bool*[Nnode];
				for(int j=0 ; j < Nnode ; j++){
				//	cout << "idx[" << j << "]" << idx[j] <<endl;
					c[j] = C[idx[j]];
				}
				delete[] idx;
				int** Ccum=new int*[Nnode];
				for(int j=0; j<Nnode; j++)
					Ccum[j]=new int[nclasses];

				for(int j2=0; j2<nclasses; j2++)
					if(c[0][j2])
						Ccum[0][j2] = 1;
					else
						Ccum[0][j2] = 0;
				for(int j=1 ; j < Nnode ; j++){
					for(int j2=0; j2<nclasses; j2++){
						Ccum[j][j2] = Ccum[j-1][j2];
						if(c[j][j2])
							Ccum[j][j2]++;
					}
				}

				float maxeps = max(eps(x[0]),eps(x[Nnode-1]));
				if(x[0]+maxeps > x[Nnode-1])
					continue; //DO WE NEED THIS
				vector<int> rows;
 				for(int j=1; j < Nnode ; j++){
					if(x[j-1]+maxeps < x[j])
						rows.push_back(j-1);
				}
				if(rows.size()==0)
					continue;

				/*
				 * DO WE NEED THIS - minleaf==1 ?
				% Reduce the list of splits to use only rows with enough class
				         % counts. For categorical vars need to consider all split
				         % permutations => reducing Ccum based on row counts doesn't work
				         if ~xcat && minleaf>1
				             Ctot = sum(Ccum(end,:));% total counts from all classes
				             Crow = sum(Ccum,2);% cumsum of class counts on each row
				             rows = rows( Crow(rows)>=minleaf & (Ctot-Crow(rows))>=minleaf );
				             if isempty(rows)
				                 continue;
				             end
				         end*/
				/*
				  % For classification, keep only rows which have more than one
				         % class between them
				         if doclass
				             nrow = length(rows);
				             keep = false(nrow,1);
				             % Check between 1st instance and 1st kept row
				             if any(any(c(1:rows(1)-1,:) ~= c(2:rows(1),:),2))
				                 keep(1) = true;
				             end
				             % Check all kept rows
				             for ir=1:nrow-1
				                 if any(any(c(rows(ir):rows(ir+1)-1,:) ~= ...
				                         c(rows(ir)+1:rows(ir+1),:),2))
				                     keep(ir:ir+1) = true;
				                 end
				             end
				             % Check between last kept row and last instance
				             Nx = length(x);
				             if any(any(c(rows(end):Nx-1,:) ~= c(rows(end)+1:Nx,:),2))
				                 keep(end) = true;
				             end
				             % Reduce the list of rows
				             rows = rows(keep);
				         end
				 */
				/*for(int j=0; j<Nnode; j++){
					cout<< endl <<"c[" << j << "]:" ;				
					for(int k=0; k<nclasses; k++)
						cout<< " " << c[j][k];
				}*/
				bool* keep=new bool[rows.size()];
				for(unsigned int j=0; j< rows.size(); j++)
					keep[j]=false;
				for(int j=0 ; j < rows.at(0) ; j++){
					for(int k=0 ;  k < nclasses ; k++){
						if(c[j][k] != c[j+1][k]){
							keep[0]=true;
						}
					}
				}
				//cout << "keep[0]:" << keep[0] <<endl;
				bool brk=false;
				for(unsigned int ir = 0 ; ir < rows.size()-1 ; ir++){
					brk=false;
					for(int j=rows.at(ir) ; j < rows.at(ir+1) && brk==false ; j++){
						for(int k=0 ;  k < nclasses ; k++){
							if(c[j][k] != c[j+1][k]){
								keep[ir]=true;
								keep[ir+1]=true;
								brk=true;
								break;
							}
						}
					}
				}
				//for(unsigned int zzz=0; zzz< rows.size(); zzz++)
				//	cout<< "keep[" << zzz << "]:" << keep[zzz] << endl;
				int Nx = Nnode; //Nnode is the length of xs
				for(int k= rows.at( rows.size()-1 ) ; k < Nx-1 ; k++){
					for(int jj=0 ; jj < nclasses ; jj++){
						if(c[k][jj] != c[k+1][jj])
							keep[rows.size()-1] = true;
					}
				}
				//cout<< "keep[end]" << keep[rows.size()-1]<< endl;
				vector<int>::iterator it;
				int k=0;
				for(it =  rows.begin(); it < rows.end() ;){
					if(!keep[k])
						it = rows.erase(it);
					else
						it++;
					k++;
				}
				delete[] keep;
				//int Ccumcpy[Nnode][nclasses];
				int** Ccumcpy=new int*[Nnode];
				for(int ii=0; ii<Nnode; ii++){
					Ccumcpy[ii]=new int[nclasses];
					for(int j=0 ; j<nclasses ; j++){
						Ccumcpy[ii][j] = Ccum[ii][j];
					}
				}
				float* crit_cut_val = RCcritval(x,c,Ccumcpy, rows, pratio, sumPjandt, bestcrit, minleaf, Nnode, nclasses);
				delete[] x;
				delete[] c;
				for(int ii=0; ii<Nnode; ii++){
					delete[] Ccumcpy[ii];
					delete[] Ccum[ii];
				}
				delete[] Ccumcpy;
				delete[] Ccum;

				/*
				 * % Change best split if this one is best so far
			if critval>bestcrit
				bestcrit = critval;
				bestvar = jvar;
				bestcut = cutval;
			 end
				 */
				float b_crit= (float)((int) (bestcrit*10000)) / 10000;
				float crit_c_v= (float)((int) (crit_cut_val[0]*10000)) / 10000; 
				//if(crit_cut_val[0] > bestcrit){
				if(crit_c_v > b_crit){
					bestcrit = crit_cut_val[0];
					bestvar = i; //jvar is i
					bestcut =  crit_cut_val[1];
				}
				delete[] crit_cut_val;
				/*
				 * % If there are missing values, recompute quantities
         %   needed for splits ignoring these
         Pt0 = [];
         ybar0 = [];
         if doclass
             Pt0 = Pt;
         else
             ybar0 = ybar;
         end
         if any(idxnan)
             if doclass
                 Njt0 = sum(c,1);
                 Pjandt0 = Prior .* Njt0 ./ max(1,Nj);
                 Pt0 = sum(Pjandt0);
             else
                 ybar0 = mean(c);
             end
         end
				 */
				///////////////////////////////////////////////////
			} //END OF BIG FOR
				///////////////////////////////////////////////////

			/*
			 * % Split this node using the best rule found
		   if bestvar~=0
			 x = Xnode(:,bestvar);
			 if ~iscat(bestvar)
				cutvar(tnode) = bestvar;
				leftside = x<=bestcut;
				rightside = ~leftside;
			 else
				cutvar(tnode) = -bestvar;          % negative indicates cat. var. split
				leftside = ismember(x,bestcut{1});
				rightside = ismember(x,bestcut{2});
			 end
			 cutpoint{tnode} = bestcut;
			 children(tnode,:) = nextunusednode + (0:1);
			 assignednode{nextunusednode} = noderows(leftside);
			 assignednode{nextunusednode+1} = noderows(rightside);
			 nodenumber(nextunusednode+(0:1)) = nextunusednode+(0:1)';
			 parent(nextunusednode+(0:1)) = tnode;
			 nextunusednode = nextunusednode+2;
		  end
	   end
	   tnode = tnode + 1;
	end
			 */
			vector<DataVar*> x;
			if(bestvar != -1){
				vector<bool> left_right_side; //a "true" value means this index should be right, otherwise go left
				for(unsigned int i=0 ; i < assignednode.at(tnode).size(); i++ )
					x.push_back( data->at(assignednode.at(tnode).at(i)).at(bestvar) );

				cutvar[tnode] = bestvar;
				for(unsigned int i=0 ; i < x.size() ; i++){
					if( x.at(i)->getType()==NUMERIC && x.at(i)->getNumVal() <= bestcut )
						left_right_side.push_back(false);
					else
						left_right_side.push_back(true);
				}
				cutpoint[tnode] = bestcut;
				children[0][tnode] = nextunusednode;
				children[1][tnode] = nextunusednode + 1;
				for(unsigned int i=0 ; i < assignednode.at(tnode).size() ; i++){
					if(!left_right_side[i])
						assignednode.at(nextunusednode).push_back(assignednode.at(tnode).at(i));
					else
						assignednode.at(nextunusednode+1).push_back(assignednode.at(tnode).at(i));
				}

				parent[nextunusednode] = tnode;
				parent[nextunusednode+1] = tnode;
				nextunusednode +=2;
				nodenumber=nextunusednode;
			}

		}
		tnode++;
	}
	for(int i=0; i<nclasses; i++)
			delete[] cost[i];
	delete[] cost;
	delete[] nj;
	delete[] pratio;
	delete[] prior;
	for(unsigned int i=0; i<data->size(); i++)
		delete[] C[i];
	delete[] C;
	//DEBUG PRINTOUT:
	/*for(int i=0; i<nodenumber; i++)
		cout<< "cutvar[" << i << "]:" << cutvar[i]<<endl;*/
	mergeLeaves();
}

float* DecisionTree::RCcritval(float x[],bool* [],int* Ccum[], vector<int> rows,
								float pratio[],float Pt, float bestcrit,int ,
								int Nnode, int nclasses){
	/*
	 * % How many splits?
	nsplits = length(rows); 	//we'll just use rows.size()

	% Split between each pair of distinct ordered values
   Csplit1 = Ccum(rows,:);
	 */
	int** Csplit1=new int*[rows.size()];
	for(unsigned int i=0; i<rows.size(); i++){
		Csplit1[i]=Ccum[rows.at(i)];
	}
	/*
	% Complementary splits
	Csplit2 = Ccum(size(Ccum,1)*ones(nsplits,1),:) - Csplit1;
	 */
	int** Csplit2=new int*[rows.size()];
	for(unsigned int i=0; i<rows.size(); i++){
		Csplit2[i]=new int[nclasses];
		for(int j=0; j<nclasses; j++)
			Csplit2[i][j]=Ccum[Nnode-1][j]-Csplit1[i][j];
	}
	/*
	% Classification
    temp = pratio(ones(nsplits,1),:); %repmat(pratio,nsplits,1);
    P1 = temp .* Csplit1;
    P2 = temp .* Csplit2;
    Ptleft  = sum(P1,2);
    Ptright = sum(P2,2);
    nclasses = size(P1,2);		//hagdara ma'agalit?!
    wuns = ones(1,nclasses);
    P1 = P1 ./ Ptleft(:,wuns);   %repmat(Ptleft,1,nclasses);
    P2 = P2 ./ Ptright(:,wuns);  %repmat(Ptright,1,nclasses);

    % Get left/right node probabilities
    Pleft = Ptleft ./ Pt;
    Pright = 1 - Pleft;

    % Evaluate criterion as impurity or otherwise
    if isimpurity 			//in our code isimpurity is always 1.
        crit = - Pleft.*feval(critfun,P1);
        t = (crit>bestcrit); % compute 2nd term only if it would make a difference
        if any(t)
            crit(t) = crit(t) - Pright(t).*feval(critfun,P2(t,:));
        end

	 */
	float** temp=new float*[rows.size()];
	float** P1=new float*[rows.size()];
	float** P2=new float*[rows.size()];
	float* Ptleft=new float[rows.size()];
	float* Ptright=new float[rows.size()];
	for(unsigned int i=0; i<rows.size(); i++){
		temp[i]=new float[nclasses];
		P1[i]=new float[nclasses];
		P2[i]=new float[nclasses];
		Ptleft[i]=0;
		Ptright[i]=0;
		for(int j=0; j<nclasses; j++){
			temp[i][j]=pratio[j];
			P1[i][j]=temp[i][j] * Csplit1[i][j];
			P2[i][j]=temp[i][j] * Csplit2[i][j];
			Ptleft[i]+=P1[i][j];
			Ptright[i]+=P2[i][j];
		}
	}
	delete[] Csplit1;
	for(unsigned int i=0; i<rows.size(); i++)
		delete[] Csplit2[i];
	delete[] Csplit2;
	float* Pleft=new float[rows.size()];
	float* Pright=new float[rows.size()];
	for(unsigned int i=0; i<rows.size(); i++){
		for(int j=0; j<nclasses; j++){
			P1[i][j]= P1[i][j] / Ptleft[i];
			P2[i][j]= P2[i][j] / Ptright[i];
		}
		Pleft[i]=Ptleft[i] / Pt;
		Pright[i]= 1- Pleft[i];
	}

	float* crit=new float[rows.size()];
	for(unsigned int i=0; i<rows.size(); i++){
		crit[i]= -Pleft[i]*gdi(P1[i],nclasses);
		if(crit[i]> bestcrit)
			crit[i]-= Pright[i]*gdi(P2[i],nclasses);
	}

	for(unsigned int i=0; i<rows.size(); i++){
		delete[] temp[i];
		delete[] P1[i];
		delete[] P2[i];
	}
	delete[] temp;
	delete[] P1;
	delete[] P2;
	delete[] Ptleft;
	delete[] Ptright;
	delete[] Pleft;
	delete[] Pright;
	/*
	 * % Get best split point
	[critval,maxloc] = max(crit);
	if critval<bestcrit
	   cutval = NaN;		///we'll check it outside the function
	   return;
	end

	% Get the cut value
	if xcat 						//xcat is always false
	   t = logical(A(maxloc,:));
	   xleft = allx(t);
	   xright = allx(~t);
	   cutval = {xleft' xright'};
	else
	   cutloc = rows(maxloc);
	   cutval = (x(cutloc) + x(cutloc+1))/2;
	end
	 *
	 */
	int maxloc=0;
	float critval=crit[0];
	for(unsigned int i=1; i<rows.size(); i++){
		if(crit[i]>critval){
			critval=crit[i];
			maxloc=i;
		}
	}
	delete[] crit;
	int cutloc = rows.at(maxloc);
	float* ans = new float[2];
	ans[0] = critval;
	ans[1] = (x[cutloc] + x[cutloc+1])/2;
	return ans;
}






/*
 * function v=gdi(p)
%GDI Gini diversity index

v=1-sum(p.^2,2);
end*/

float DecisionTree::gdi(float p[],int length){
	float ans=0;
	for(int i=0 ; i<length ; i++){
		ans+= pow(p[i],2);
	}
	return 1-ans;
}
/**
 * input: two arrays, initialized. "arr" is the array to sort.
 * "indices" is first simply the indices 1..arr.size() and is shuffled the same as arr.
 * size holds arr's (and thus also indices's) length
 */
void DecisionTree::sort(float arr[], int indices[],int size){
	boubleSort(arr,indices,size-1);
}

void DecisionTree::quickSort(float arr[], int indices[], int left, int right){
	if(right > left){
		//here we select the pivot Index to be the middle
		int pivotNewIndex= partition(arr,indices, left, right, (int)(left+right)/2);
		quickSort(arr,indices, left, pivotNewIndex-1);
		quickSort(arr,indices, pivotNewIndex+1, right);
	}
}

void DecisionTree::boubleSort(float arr[], int indices[],int size){
	for(int i=size; i > 0; i--)
		for(int j=0; j < i; j++){
			if(arr[j] > arr[j+1]){
				float tmp = arr[j];
				int tmpIdx = indices[j];
				arr[j]=arr[j+1];
				indices[j] = indices[j+1];
				arr[j+1] = tmp;
				indices[j+1] = tmpIdx;
			}
		}
}

int DecisionTree::partition(float arr[], int indices[], int left,
		int right, int pivotInd){
	float pivotValue= arr[pivotInd];
	swap(arr,indices,pivotInd,right); // Move pivot to end
	int storeIndex= left;
	for(int i=left; i< right; i++){
		if(arr[i] <= pivotValue){
			swap(arr,indices,storeIndex,i);
			storeIndex++;
		}
	}
	swap(arr,indices,right,storeIndex); // Move pivot to its final place
	return storeIndex;
}

void DecisionTree::swap(float arr[], int indices[], int first,int second){
	float tmpA=arr[first];
	int tmpIdx=indices[first];

	arr[first]=arr[second];
	indices[first]=indices[second];

	arr[second]=tmpA;
	indices[second]=tmpIdx;
}

// Calculates log2 of number.  
double Log2( double n )  
{  
    // log(n)/log(2) is log2.  
    return log( n ) / log( (double)2 );  
}

/**
 * eps(num) is the positive distance from abs(num) to the next
 * larger in magnitude floating point number of the same precision as num.
 * num is a single precision number (float).
 */
float DecisionTree::eps(float num){
	return (float)pow(2,-23+(floor (Log2(fabs(num))) ));
}
/*
ostream& operator<<(ostream& out, const DecisionTree& r){
  return out<< r.toString();
}*/


string DecisionTree::toString() const{
	/*
	 for j=1:maxnode  //maxnode is nodenumber
    if any(t.children(j,:))
        % branch node
        vnum = t.var(j);
        vname = names{abs(vnum)};
        cut = t.cut{j};
        kids = t.children(j,:);
        if vnum>0        % continuous predictor "<" condition
            cond = sprintf('%s<%g',vname,cut);
            fprintf('%*d  if %s then node %d else node %d\n',nd,j,cond,kids);
        else             % categorical predictor, membership condition
            cats = cut{1};
            if isscalar(cats)
                condleft = sprintf('%s=%g',vname,cats);
            else
                set = deblank(num2str(cats,'%g '));
                condleft = sprintf('%s in {%s}',vname,set);
            end
            cats = cut{2};
            if isscalar(cats)
                condright = sprintf('%s=%g',vname,cats);
            else
                set = deblank(num2str(cats,'%g '));
                condright = sprintf('%s in {%s}',vname,set);
            end
            if     strcmp(type(t),'regression')
                Yfit = t.class(j);
            elseif strcmp(type(t),'classification')
                Yfit = t.classname{t.class(j)};
            end
            Yfit = num2str(Yfit,'%g');
            fprintf('%*d  if %s then node %d elseif %s then node %d else %s\n',...
                nd,j,condleft,kids(1),condright,kids(2),Yfit);
        end
    else
        % terminal node, display fit (regression) or class assignment
        if isregression
            fprintf('%*d  fit = %g\n',nd,j,t.class(j));
        else
            fprintf('%*d  class = %s\n',nd,j,t.classname{t.class(j)});
        end
    end
end
	 */
	ostringstream ans(ostringstream::out);//=new ostringstream();
	ans << "Decision tree:\n";
	for(int j=0; j< nodenumber; j++){
		if(this->children[0][j]!=0 || this->children[1][j]!=0){//branch node
			int vnum=cutvar[j];
			DataVar* vname=attributes->at(vnum).at(0);
			//float cut=cutpoint[j];
			if(vnum>=0){
				ans<< j << "\t if "<< vname->getStrVal() << "<" << cutpoint[j] <<" then node "<< children[0][j] <<" else node "
					<< children[1][j]<< endl;
			}//else non-continuous variable: unsupported for now.
		}else{//terminal node, display class assignment
			DataVar* cname=attributes->at(attributes->size()-1).at(yfitnode[j]+1);
			ans<< j << "\t class = " << cname->getStrVal() << endl;
		}
	}
	return ans.str();
}
/*
 * @param ind is node ind
 * -1 as input will return all err info.
 */
string DecisionTree::getnodeerr(int ind) const{
	ostringstream ans(ostringstream::out);
	if(ind<0){
		ans << "tree err data:" << endl;
		for(int i=0; i<nodenumber; i++)
			ans << "node " << i << ":" << this->resuberr[i] << endl;
	}else if(ind>=nodenumber)
		ans << "no such node. max index is: " << nodenumber-1 <<endl;
	else
		ans << "node "<< ind <<":" << this->resuberr[ind] <<endl;
	return ans.str();
}

/*
 * @param ind is node ind
 * -1 as input will return all classcount info.
 */
string DecisionTree::getclasscount(int ind) const{
	ostringstream ans(ostringstream::out);
	if(ind<0){
		ans << "tree classcount data:" << endl;
		for(int i=0; i<nodenumber; i++){
			ans << "node " << i << ":";
			for(int j=0; j<nclasses; j++)
				ans << this->classcount[i][j] << " ";
			ans<< endl;
		}
	}else if(ind>=nodenumber)
		ans << "no such node. max index is: " << nodenumber-1 <<endl;
	else{
		ans << "node "<< ind <<":";
		for(int j=0; j<nclasses; j++)
			ans << this->classcount[ind][j] << " ";
		ans<< endl;
	}

	return ans.str();
}

string DecisionTree::eval(vector< vector<DataVar*> > *evaldata) const{
	ostringstream ans(ostringstream::out);
	for(unsigned int i=0; i< evaldata->size(); i++){
		int j=0;
		for(;;){
			if(this->children[0][j]!=0 || this->children[1][j]!=0){//branch node
				int vnum=cutvar[j];
				if(vnum>=0){
					if(evaldata->at(i).at(vnum)->getNumVal() < cutpoint[j])
						j=children[0][j];
					else
						j=children[1][j];
				}//else non-continuous variable: unsupported for now.
			}else{//terminal node, display class assignment
				DataVar* cname=attributes->at(attributes->size()-1).at(yfitnode[j]+1);
				ans<< i+1<< ": "<< cname->getStrVal()<< " on node: " << j << ", nodeerr: "<< this->resuberr[j]<< endl;
				break;
			}
		}
	}
	return ans.str();
}

string DecisionTree::eval(const std::string& dataFile) const{
	vector< vector<DataVar*> > evaldata;
	unsigned int ind=0,i=0; //this one helps determine which line gets token.
	vector<DataVar*> *dataLn;
	string line,word;
	DataVar* token;
	ifstream myfile(dataFile.c_str(),ifstream::in);
	if (myfile.is_open()){
		while (! myfile.eof() ){
			getline (myfile,line);
			ind=0;
			dataLn= new vector<DataVar*>();
			while(line.length()>0 && dataLn->size()<attributes->size()-1 ){
				if(line.at(0)==' ' || line.at(0)=='\t'){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				word=line.substr(0,line.find(',',0));
				if(word.length()==line.length()){
					line="";
					if(ind < attributes->size()-2){
						return "missing data in record. aborting.\n";
					}
				}else{
					line=line.substr(line.find(',',0)+1);
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\t')
					word=word.substr(0,word.length()-1);

				if(attributes->at(ind).size()==1)//continuous variable
					token=new DataVar(NUMERIC,word);//tokenize(word);
				else{
					for(i=1; i<attributes->at(ind).size();i++)
						if(strcmp(word.c_str(),
								attributes->at(ind).at(i)->getStrVal().c_str())==0){
							token=attributes->at(ind).at(i);//TODO - change here like on "change 11"
							break; //for
						}
					if(i >= attributes->at(ind).size()){
						cout<<"invalid value on "<< word.c_str() << " aborting."<<endl;
						return "";
					}
				}//token is now the next data element to add
				if(ind==attributes->size()-1)
					break; //hence get next line
					//throw("too many values in record");
				dataLn->push_back(token);
				ind++;
			}
			if(dataLn->size()>0)
				evaldata.push_back(*dataLn);
			else
				dataLn->~vector();
		}
		myfile.close();
	}
	else{
		cout <<"Unable to open dataFile" << endl;
		return "";
	}
	return eval(&evaldata);
}

string DecisionTree::getEvalData(vector<DataVar*> *toEval,int* classification, float* nodePercentage, float* nodeErr){
	ostringstream ans(ostringstream::out);
	int j=0;
	for(;;){
		if(this->children[0][j]!=0 || this->children[1][j]!=0){//branch node
			int vnum=cutvar[j];
			if(vnum>=0){
				if(toEval->at(vnum)->getNumVal() < cutpoint[j])
					j=children[0][j];
				else
					j=children[1][j];
			}//else non-continuous variable: unsupported for now.
		}else{//terminal node, display class assignment
			DataVar* cname=attributes->at(attributes->size()-1).at(yfitnode[j]+1);
			ans<< cname->getStrVal() << " on node: " << j << endl;
			*classification=yfitnode[j]+1; 
			//sets classification (index of class in attributes->at(attributes->size()-1) )
			int clscnt=0;
			for(int k=0; k<nclasses; k++)
				clscnt+=this->classcount[j][k];
			*nodePercentage=(float)clscnt / this->data->size(); 
			//sets nodePercentage to be: classcount at this node (amount of records classified in it) divided by total records. 
			*nodeErr= this->resuberr[j];
			//sets nodeErr to the correct value.
			break;
		}
	}
	return ans.str();
}

void DecisionTree::mergeLeaves(){
/*
 %MERGELEAVES Merge leaves that originate from the same parent node and give
 % the sum of risk values greater or equal to the risk associated with the
 % parent node.

N = length(Tree.node); 												//nodenumber
isleaf = (Tree.var==0)';   % no split variable implies leaf node 	// cutvar[i]!=0
isntpruned = true(1,N);
doprune = false(1,N);
Risk = risk(Tree)';
adjfactor = (1 - 100*eps(class(Risk)));

% Work up from the bottom of the tree
for(;;)
   % Find ''twigs'' with two leaf children
   branches = find(~isleaf & isntpruned);
   twig = branches(sum(isleaf(Tree.children(branches,:)),2) == 2);
   if isempty(twig)
      break;            % must have just the root node left
   end

   % Find twigs to ''unsplit'' if the error of the twig is no larger
   % than the sum of the errors of the children
   Rtwig = Risk(twig);
   kids = Tree.children(twig,:);
   Rsplit = sum(Risk(kids),2);
   unsplit = Rsplit >= Rtwig'*adjfactor;
   if any(unsplit)
      % Mark children as pruned, and mark twig as now a leaf
      isntpruned(kids(unsplit,:)) = 0;
      twig = twig(unsplit);   % only these to be marked on next 2 lines
      isleaf(twig) = 1;
      doprune(twig) = 1;
   else
      break;
   end
end

% Remove splits that are useless
if any(doprune)
   Tree = prune(Tree,'nodes',find(doprune));
end
end
	 */
	bool* isntpruned=new bool[nodenumber];
	bool* doprune=new bool[nodenumber];
	bool* isleaf=new bool[nodenumber];
	for(int i=0; i<nodenumber; i++){
		isntpruned[i]=true;
		doprune[i]=false;
		isleaf[i]= (cutvar[i]==-1);//(cutvar[i]!=-1/*0*/);
	}
	float* Risk=risk();
	float adjfactor = (1 - 100*eps(1.0));
	for(;;){
		vector<int> branches;
		for(int i=0; i<nodenumber; i++){
			if(!isleaf[i] && isntpruned[i])//(isleaf[i]!=0 && isntpruned[i])
				branches.push_back(i);
		}
		vector<int> twig; //better to just use branches and delete nodes (or never inserting them)
		for(unsigned int i=0; i<branches.size(); i++)
			//if(cutvar[children[0][branches.at(i)]]==-1/*0*/ && cutvar[children[1][branches.at(i)]]==-1/*0*/)
			if(isleaf[children[0][branches.at(i)]] && isleaf[children[1][branches.at(i)]])
				twig.push_back(branches.at(i));
		if(twig.empty())
			break;
		//float* Rtwig=new float[twig.size()];//=Risk(twig);
		vector<int>* kids=new vector<int>[twig.size()];
		bool _break=true;
		for(unsigned int i=0; i<twig.size(); i++){
			kids[i].push_back(children[0][twig.at(i)]);
			kids[i].push_back(children[1][twig.at(i)]);
			//Rtwig[i]=Risk[twig.at(i)];
			float Rsplit=0;
			for(unsigned int j=0; j<kids[i].size(); j++)
				Rsplit+=Risk[kids[i].at(j)];
			if(Rsplit >= /*Rtwig[i]*/Risk[twig.at(i)] * adjfactor){
				_break=false;
				isntpruned[kids[i].at(0)]=0;
				isntpruned[kids[i].at(1)]=0;
				isleaf[twig.at(i)]=1;//0;
				doprune[twig.at(i)]=1;
			}
		}
		delete[] kids;
		if(_break)
			break;
	}
	delete[] Risk;
	delete[] isntpruned;
	delete[] isleaf;
	//DEBUG PRINT:
	//cout<< this->toString() << endl;

	//we send a bool[nodenumber] where nodes with 'true' value should be pruned
	this->prune(doprune);
	delete[] doprune;
}
/**
 * returns an N-element vector R of the risk of the nodes in the
%   tree T, where N is the number of nodes.
 */
float* DecisionTree::risk(){
	/*
	 *  r = t.nodeprob .* t.nodeerr;
	 */
	float* ans=new float[nodenumber];
	for(int i=0; i<nodenumber; i++){
		ans[i]=this->nodeprob[i] * this->resuberr[i];
	}
	return ans;
}

/**
 * T2 = PRUNE(T1,'nodes',NODES) prunes the nodes listed in the NODES
%   vector from the tree.  Any T1 branch nodes listed in NODES become
%   leaf nodes in T2, unless their parent nodes are also pruned.  The
%   VIEW method can display the node numbers for any node you select.
 */
void DecisionTree::prune(bool* doprune){
	vector<int> nodes;
	for(int i=0; i<nodenumber; i++)
		if(doprune[i])
			nodes.push_back(i);
	//getpruneinfo(); -- CAN WE AVOID THIS??

	/*
	 * PRUNENODES Prune selected branch nodes from tree.
	N = length(Tree.node);		//is nodenumber

	% Find children of these branches and remove them
	parents = branches;			//is nodes
	tokeep = true(N,1);
	kids = [];
	for(;;)
	   newkids = Tree.children(parents,:);
	   newkids = newkids(:);
	   newkids = newkids(newkids>0 & ~ismember(newkids,kids));
	   if isempty(newkids), break; end
	   kids = [kids; newkids];
	   tokeep(newkids) = 0;
	   parents = newkids;
	end
	 */
	bool* tokeep=new bool[nodenumber];
	bool* inkids=new bool[nodenumber];
	for(int i=0; i<nodenumber; i++){
		tokeep[i]=true;
		inkids[i]=false;
	}
	vector<int> kids;
	vector<int> *prnts,*newkids=NULL;
	prnts=new vector<int>();
	*prnts=nodes;
	while(!prnts->empty()){
		newkids=new vector<int>;
		for(unsigned int i=0; i<prnts->size(); i++){
			int kid=children[0][prnts->at(i)];
			if(kid>0 && !inkids[kid]){
				kids.push_back(kid);
				inkids[kid]=true;
				tokeep[kid]=false;
				newkids->push_back(kid);
			}
			kid=children[1][prnts->at(i)];
			if(kid>0 && !inkids[kid]){
				kids.push_back(kid);
				inkids[kid]=true;
				tokeep[kid]=false;
				newkids->push_back(kid);
			}
		}
		delete prnts;
		prnts=newkids; //just pointers...
	}
	delete prnts;
	/*
	 * % Convert branches to leaves by removing split rule and children
	Tree.var(branches) = 0;
	Tree.cut(branches) = {0};
	Tree.children(branches,:) = 0;

	% Get new node numbers from old node numbers
	ntokeep = sum(tokeep);
	nodenums = zeros(N,1);
	nodenums(tokeep) = (1:ntokeep)';
	 */

	for(unsigned int i=0; i<nodes.size(); i++){
		this->cutvar[nodes.at(i)]=0;
		this->cutpoint[nodes.at(i)]=0;
		this->children[0][nodes.at(i)]=0;
		this->children[1][nodes.at(i)]=0;
	}
	int ntokeep=0;
	int* nodenums=new int[nodenumber];

	for(int i=0; i<nodenumber; i++){
		if(tokeep[i]){
			nodenums[i]=ntokeep;
			ntokeep++;
		}else
			nodenums[i]=0;
	}

	/*
	 * % Reduce tree, update node numbers, update child/parent numbers
	Tree.parent    = Tree.parent(tokeep);
	Tree.class     = Tree.class(tokeep);
	Tree.var       = Tree.var(tokeep);
	Tree.cut       = Tree.cut(tokeep);
	Tree.children  = Tree.children(tokeep,:);
	Tree.nodeprob  = Tree.nodeprob(tokeep);
	Tree.nodeerr   = Tree.nodeerr(tokeep);
	Tree.nodesize  = Tree.nodesize(tokeep);
	Tree.node      = (1:ntokeep)'; 			//is nodenumber
	mask = Tree.parent>0;
	Tree.parent(mask) = nodenums(Tree.parent(mask));
	mask = Tree.children>0;
	Tree.children(mask) = nodenums(Tree.children(mask));
	if isequal(Tree.method,'classification') 	//always true!
		  Tree.classprob = Tree.classprob(tokeep,:);
		  Tree.classcount= Tree.classcount(tokeep,:);
		  if ~isempty(Tree.impurity)
		     Tree.impurity = Tree.impurity(tokeep,:);
		  end
	end	 */
	//create new arrays (pointers) to every class field.
	//copy relevant info; release old class members and point to new ones.
	int *_parent=new int[ntokeep];
	int *_yfitnode=new int[ntokeep];
	int *_cutvar=new int[ntokeep];
	float *_cutpoint=new float[ntokeep];
	//int **_children=new int*[2];
	int* _children0=new int[ntokeep];
	int* _children1=new int[ntokeep];
	float* _nodeprob=new float[ntokeep];
	float* _resuberr=new float[ntokeep];
	int* _nodesize=new int[ntokeep];
	float** _classprob=new float*[ntokeep];
	int** _classcount=new int*[ntokeep];
	float* _impurity=new float[ntokeep];
	int ind=0;
	for(int i=0; i<nodenumber; i++){
		if(tokeep[i]){
			_parent[ind]=parent[i];
			_yfitnode[ind]=yfitnode[i];
			_cutvar[ind]=cutvar[i];
			_cutpoint[ind]=cutpoint[i];
			_children0[ind]=children[0][i];
			_children1[ind]=children[1][i];
			_nodeprob[ind]=nodeprob[i];
			_resuberr[ind]=resuberr[i];
			_nodesize[ind]=nodesize[i];
			_classprob[ind]=classprob[i];
			_classcount[ind]=classcount[i];
			_impurity[ind]=impurity[i];
			ind++;
		}
	}

	delete[] parent;
	parent=_parent;
	delete[] yfitnode;
	yfitnode=_yfitnode;
	delete[] cutvar;
	cutvar=_cutvar;
	delete[] cutpoint;
	cutpoint=_cutpoint;
	delete[] children[0];
	children[0]=_children0;
	delete[] children[1];
	children[1]=_children1;

	delete[] nodeprob;
	nodeprob=_nodeprob;
	delete[] resuberr;
	resuberr=_resuberr;
	delete[] nodesize;
	nodesize=_nodesize;
	for(int i=0; i<m; i++){
		if(i>=nodenumber || !tokeep[i]){
			delete[] classprob[i];
			delete[] classcount[i];
		}
	}
	delete[] classprob;
	delete[] classcount;
	classprob=_classprob;
	classcount=_classcount;
	delete[] impurity;
	impurity=_impurity;

	m=nodenumber=ntokeep;
	for(int i=0; i<nodenumber; i++){
		if(parent[i]>0)
			parent[i]=nodenums[parent[i]];
		if(children[0][i]>0)
			children[0][i]=nodenums[children[0][i]];
		if(children[1][i]>0)
			children[1][i]=nodenums[children[1][i]];
	}
	delete[] tokeep;
	delete[] inkids;
	delete[] nodenums;

}


float DecisionTree::setFitness(){
	float errPcnt=0;
	for(int i=0; i<nodenumber; i++){
		if(this->children[0][i]==0 && this->children[1][i]==0){//leaf
			int max=0,clscnt=0;
			for(int k=0; k<nclasses; k++){
				if(max < this->classcount[i][k]){
					clscnt+= max;
					max=this->classcount[i][k];
				}else{
					clscnt+=this->classcount[i][k];
				}
			}
			errPcnt+=clscnt;
		}
	}
	fitness= errPcnt / data->size();
	return fitness;
}

float DecisionTree::getFitness(){
	if(this->fitness== -1) 
		setFitness();
	return this->fitness;
}