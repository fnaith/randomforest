/*
 * DataVar.cpp
 *
 *  Created on: Jun 28, 2010
 *      Author: operry5
 */
#include "../h/DataVar.h"
#include <malloc.h>
//#include <limits>

#include <math.h>

DataVar::DataVar():strVal(""),numVal(0),type(NUMERIC){}

DataVar::DataVar(float num):strVal(""),numVal(num),type(NUMERIC){}

DataVar::DataVar(DataVarType type,string& str):strVal(""),numVal(0),type(type){
	if(type==NUMERIC){
		tokenize(str);
	}else{
		strVal=str;
	}
}

DataVar::~DataVar(){}

void DataVar::setAsString(string& str){
	strVal=str;
	setType(STRING);
}

void DataVar::setAsNum(float _num){
	numVal=_num;
	setType(NUMERIC);
}

void DataVar::setType(DataVarType _type){
	type=_type;
}

//returns asked value, or if type isn't numeric - returns infinity
//MAKE SURE TYPE IS NUMERIC WHEN YOU CALL THIS FUNCTION
float DataVar::getNumVal(){
	if(type==NUMERIC)
		return numVal;
	else return 0;//numeric_limits<float>::infinity();
}

string DataVar::getStrVal(){
	if(type==STRING)
		return strVal;
	else return "";
}
/*
DataVar* makeDataVar(float* num){
	DataVar* ans=(DataVar*)malloc(sizeof(DataVar));
	ans->type=NUMERIC;
	ans->dval.num=num;
	return ans;
}

DataVar* tokenizeString(string& str){
	string* s=new string(str);
	DataVar* ans=(DataVar*)malloc(sizeof(DataVar));	//may need malloc???
	ans->type=STRING;
	ans->dval.str=s;
	return ans;
}*/

/*
 * continue parsing exponent part from 'left'. (after addFraction)
 * multiply num by 10^left and returns a DataVar* with that value.
 * upon failure (string isn't a number) calls tokenizeString(str)
 */
void /*DataVar* */ DataVar::addExp(string& str,string& left,float num,bool isNegative){
	int exp=0;
	bool isNeg=false;
	if(left.length()==0)
		throw("error:expected a num on continuous var");
	//return tokenizeString(str);
	if(left.at(0)=='-'){
		isNeg=true;
		left=left.substr(1);
	}else if(left.at(0)=='+') //is that even possible?
		left=left.substr(1);
	if(left.length()==0){
		//return tokenizeString(str);
		setAsString(str);
		return;
	}
	while(left.length()>0){
		if(left.at(0)>='0' && left.at(0)<='9'){//parse exp
			exp=10*exp+(left.at(0)-48);
			left=left.substr(1);
		}else {
			printf("error: expected a numeric value, got %s\n",str.c_str());
			setAsString(str);//throw("error:expected a num on continuous var");//return tokenizeString(str);
			return;
		}
	}
	if(isNeg)
		exp*=-1;
	num*=(float)pow((float)10,exp);
	if(isNegative)
		num*=-1;
	//return makeDataVar(num);
	setAsNum(num);
}
/*
 * continue parsing fraction part from 'left'.
 * adds fraction to num and returns a DataVar* with that value.
 * upon failure (string isn't a number) calls tokenizeString(str)
 */
/*DataVar* */void DataVar::addFraction(string& str,string& left,float num,bool isNegative){
	int powby=-1;
	while(left.length()>0){
		if(left.at(0)>='0' && left.at(0)<='9'){
			if(num<0)
				num-= (left.at(0)-48)*pow((float)10,powby);
			else
				num+= (left.at(0)-48)*pow((float)10,powby);
			left=left.substr(1);
			powby--;
		}else if(left.at(0)=='e' || left.at(0)=='E'){
			left=left.substr(1);
			/*return*/ addExp(str,left,num,isNegative);
			return;
		}else {
			printf("error: expected a numeric value, got %s\n",str.c_str());
			setAsString(str);//throw("error:expected a num on continuous var");//return tokenizeString(str);
			return;
		}//throw("error:expected a num on continuous var");//return tokenizeString(str);
	}
	if(isNegative)
		num*=-1;
	setAsNum(num);
	//return makeDataVar(num);
}
/**
 * returns a parsed float or the string itself wrapped in a DataVar struct, to be pushed into vector
 */
/*DataVar* */ void DataVar::tokenize(string& str){
	string tmp=str;
	bool isNeg=false;
	float num=0;
	if(tmp.at(0)>='0' && tmp.at(0)<='9'){
		while(tmp.length()>0){
			if(tmp.at(0)>='0' && tmp.at(0)<='9'){//complete part
				num=10*num+(tmp.at(0)-48);
				tmp=tmp.substr(1);
			}else if(tmp.at(0)=='.'){
				tmp=tmp.substr(1);
				/*return*/ addFraction(str,tmp,num,isNeg); //should continue parsing (will call tokenizeString(str) on error)
				return;
			}else {
				printf("error: expected a numeric value, got %s\n",str.c_str());
				//delete num;
				setAsString(str);//throw("error:expected a num on continuous var");//return tokenizeString(str);
				return;
			}//throw("error:expected a num on continuous var");//return tokenizeString(str);
		}
		setAsNum(num);
		//return makeDataVar(num);
	}
	else if(tmp.at(0)=='-' || tmp.at(0)=='+'){
		if(tmp.length()==1){
			printf("error: expected a numeric value, got %s\n",str.c_str());
			setAsString(str);//throw("error:expected a num on continuous var");//return tokenizeString(str);
			return;
		}//throw("error:expected a num on continuous var");//return tokenizeString(str);
		else{
			isNeg=(tmp.at(0)=='-');
			tmp=tmp.substr(1);
			while(tmp.length()>0){
				if(tmp.at(0)>='0' && tmp.at(0)<='9'){//complete part
					num=10*num+(tmp.at(0)-48);
					tmp=tmp.substr(1);
				}else if(tmp.at(0)=='.'){
					tmp=tmp.substr(1);
					/*if(str.at(0)=='-')
									(*num)*=-1; //this fails on negative fractions who's int part is 0*/ //the last comment is older
					/*return*/ addFraction(str,tmp,num,isNeg); //should continue parsing (will call tokenizeString(str) on error)
					return;
				}else{
					printf("error: expected a numeric value, got %s\n",str.c_str());
					setAsString(str);//throw("error:expected a num on continuous var");//return tokenizeString(str);
					return;
				}// throw("error:expected a num on continuous var");//return tokenizeString(str);
			}
			if(isNeg/*str.at(0)=='-'*/)
				num*=-1;
			setAsNum(num);
			//return makeDataVar(num);
		}
	}
	else if(tmp.at(0)=='.' && tmp.length()>1){
		tmp=tmp.substr(1);
		/*return*/ addFraction(str,tmp,num,isNeg);
		return;
	}
	else {
		printf("error: expected a numeric value, got %s\n",str.c_str());
		setAsString(str);//throw("error:expected a num on continuous var");//return tokenizeString(str);
	}//throw("error:expected a num on continuous var");//return tokenizeString(str);
}
