#include "../h/EvoForest.h"
#include "../h/DataVar.h"
#include <iostream>
#include <fstream>
#include <sstream> 
#include <string>
#include <time.h>
#include <cstdlib>
#include <cstring>

EvoForest::~EvoForest(){
	for(unsigned int i=0; i<this->size; i++){
		this->forest[i]->clearAtrNdata();
		delete this->forest[i];
	}
	delete[] forest;
}

EvoForest::EvoForest(int generations, unsigned int _size, unsigned int attrsPerTree, 
					 vector< vector<DataVar*> > *attrs, vector< vector<DataVar*> > *data):
					minPercentage(0),forest(),size(_size),attributes(attrs),data(data){
	if(size % 2 !=0){
		size++;
		cout<< "EvoForest size must be even. size was set to "<<size <<endl;
	}
	forest=new DecisionTree*[size];
	for(unsigned int i=0; i<size; i++){
		//randomly select attrsPerTree attributes, and make tree with them:
		srand((unsigned int)time(NULL));
		vector<int> chosen;
		while(chosen.size()<attrsPerTree){
			int j=rand()% (attributes->size()-1); //draw a random attribute (last one is classifications thus -1)
			bool found=false;
			for(unsigned int k=0; k<chosen.size();k++)
				if(chosen.at(k)==j){
					found=true;
					break;
				}
			if(!found)
				chosen.push_back(j);
		}
		vector<vector <DataVar*> > *att=new vector<vector <DataVar*> >(),*dat=new vector<vector <DataVar*> >();
		//for(unsigned int j=0; j<data.size();j++)
		dat->resize(data->size());
		for(unsigned int j=0; j<attrsPerTree;j++){
			att->push_back(attributes->at(chosen.at(j)));
			for(unsigned int k=0; k<data->size(); k++)
				dat->at(k).push_back(data->at(k).at(chosen.at(j)));
		}
		att->push_back(attributes->back());
		for(unsigned int k=0; k<data->size(); k++)
			dat->at(k).push_back(data->at(k).back());

		forest[i]=new DecisionTree(att,dat);
	}
	evolution(generations);
}

EvoForest::EvoForest(const EvoForest& ){
	cout<< "ERROR: EvoForest use of copy constructor"<< endl;
}

EvoForest& EvoForest::operator=(const EvoForest& t){
	cout<< "ERROR: EvoForest use of operator="<< endl;
	EvoForest *f=new EvoForest(t);
	return *f;
}

string EvoForest::toString() const{
	ostringstream ans(ostringstream::out);
	ans << "evolution forest print-out:\n";
	for(unsigned int j=0; j< size; j++){
		ans << "(" << j << ") " << this->forest[j]->toString();
	}
	return ans.str();
}

/**
 * takes a full table record. returns a record containing only it's relevant columns 
 * in their correct order for tree: forest[treeInd]
 */
vector<DataVar*>& EvoForest::setToEval(vector<DataVar*>* evalRec,unsigned int treeInd) const{
	vector<DataVar*>* ans=new vector<DataVar*>();
	const vector<vector<DataVar*> > treeAtt=forest[treeInd]->getattributes();
	for(unsigned int i=0; i<treeAtt.size()-1; i++){
		bool found=false;
		for(unsigned int j=0; j< this->attributes->size()-1 && !found; j++){
			if(treeAtt.at(i).at(0) == this->attributes->at(j).at(0)){
				found=true;
				ans->push_back(evalRec->at(j));
			}
		}
	}
	return *ans;
}

/**
 * eval function. receives a table of unclassified records 
 * calculates majourity decision for each record.
 */
string EvoForest::eval(vector< vector<DataVar*> > *evaldata) const{
	//TODO - how do we make a majority decision?
	ostringstream ans(ostringstream::out);
	for(unsigned int i=0; i<evaldata->size(); i++){//for each record:
		ans << "line " << i << ": " ;
		float* classScore=new float[getNclasses()];	//keeps votes for each classification
		for(int j=0; j<getNclasses(); j++)
			classScore[j]=0;
		for(unsigned int j=0; j < size; j++){//for each tree: add it's eval data
			int classification; //will hold classification
			float nodepercentage,nodeErr;	//will hold classcount divided by totalRecords, and nodeErr.
			vector<DataVar*> toEval=setToEval(&(evaldata->at(i)),j); //sets toEval for tree i.
			
			forest[j]->getEvalData(&toEval,&classification, &nodepercentage, &nodeErr); //update last 3 params sent.
			if(nodepercentage >= minPercentage){//otherwise ignore this decision...
				classScore[classification-1]+= (1-nodeErr); //can add to classScore[curr_classification] up to 1, according to tree's certainty.
			}
		}
		float maxVal=0; //we'll now check which classification won the most pts.
		int decision=0;
		float pct=0;
		for(int j=0; j<getNclasses(); j++){
			if(classScore[j] > maxVal){
				decision=j;
				maxVal=classScore[j];
			}
			pct+=classScore[j]; //this first sums all scores.
		}
		pct= classScore[decision] / pct; // divide decision by total scores to get certainty percentage.
		ans << this->attributes->back().at(decision+1)->getStrVal() << " with certainty of " << pct << endl;
		delete[] classScore;
	}
	return ans.str();
}

/**
 * this function reads from file a record table to eval. 
 * uses the other 'eval' function to return answers.
 */
string EvoForest::eval(const std::string& dataFile) const{
	ostringstream ans(ostringstream::out);
	vector< vector<DataVar*> > evaldata;
	unsigned int ind=0,i=0; //this one helps determine which line gets token.
	vector<DataVar*> *dataLn;
	string line,word;
	DataVar* token;
	ifstream myfile(dataFile.c_str(),ifstream::in);
	if (myfile.is_open()){
		while (! myfile.eof() ){
			getline (myfile,line);
			ind=0;
			dataLn= new vector<DataVar*>();
			while(line.length()>0 && dataLn->size()<attributes->size()-1 ){
				if(line.at(0)==' ' || line.at(0)=='\t'){//ignore initial space(s)
					line=line.substr(1);
					continue;
				}
				word=line.substr(0,line.find(',',0));
				if(word.length()==line.length()){
					line="";
					if(ind < attributes->size()-2){
						return "missing data in record. aborting.\n";
					}
				}else{
					line=line.substr(line.find(',',0)+1);
				}
				while(word[word.length()-1]==' ' || word[word.length()-1]=='\t')
					word=word.substr(0,word.length()-1);

				if(attributes->at(ind).size()==1)//continuous variable
					token=new DataVar(NUMERIC,word);//tokenize(word);
				else{
					for(i=1; i<attributes->at(ind).size();i++)
						if(strcmp(word.c_str(),
								attributes->at(ind).at(i)->getStrVal().c_str())==0){
							token=attributes->at(ind).at(i);//TODO - change here like on "change 11"
							break; //for
						}
					if(i >= attributes->at(ind).size()){
						cout<< "invalid value on " << word.c_str() <<" aborting."<< endl;
						return "";
					}
				}//token is now the next data element to add
				if(ind==attributes->size()-1)
					break; //hence get next line
					//throw("too many values in record");
				dataLn->push_back(token);
				ind++;
			}
			if(dataLn->size()>0)
				evaldata.push_back(*dataLn);
			else
				dataLn->~vector();
		}
		myfile.close();
	}
	else return "Unable to open dataFile\n";
	return eval(&evaldata);
}




/**
 * finds _att in forest attributes. returns it's index (for data copy)
 */
unsigned int EvoForest::findAttInd(vector<DataVar*> _att){
	unsigned int j=0;
	for(;j< this->attributes->size()-1; j++){
		if(_att.at(0) == this->attributes->at(j).at(0))
			break;
	}
	return j;
}

/**
 * to assist with crossover, this method returns 'true' if attributes table 'att' has a record 
 * that is not found in 'otherAtt'
 */
bool EvoForest::hasDifferentAttrs(vector< vector<DataVar*> > &att,vector< vector<DataVar*> > &otherAtt){
	for(unsigned int i=0; i<att.size()-1; i++){
		bool found=false;
		for(unsigned int j=0; j<otherAtt.size()-1 && !found; j++)
			if(att.at(i).at(0)==otherAtt.at(j).at(0))
				found=true;
		if(!found)
			return true;
	}
	return false;
}

/**
 * pick an arbitrary attribute from each tree, switch between them, and create the new trees
 */
void EvoForest::crossover(int ind1,int ind2){
	/*DecisionTree* t1=forest[ind1];
	DecisionTree* t2=forest[ind2];*/
	vector< vector<DataVar*> > *att1, *att2, *data1, *data2;

	vector< vector<DataVar*> > oldatt1 =forest[ind1]->getattributes();
	vector< vector<DataVar*> > oldatt2 =forest[ind2]->getattributes();
	bool found=false; 
	int i1=-1; //attr idx to obtain from t1 to t2.
	srand((unsigned int)time(NULL));
	if(hasDifferentAttrs(oldatt1,oldatt2)){
		while(!found){ //if 'found' then 'ind' is the newly chosen index to add.
			found=true; //assume we'll find in this iteration.
			i1=rand()% (oldatt1.size()-1); //draw a random attribute (last one is classifications thus -1)
			for(unsigned int k=0; k < oldatt2.size(); k++)
				if(oldatt2.at(k).at(0)==oldatt1.at(i1).at(0)){
					found=false;
					break; //to re-draw
				}
		}
	}

	found=false; 
	int i2=-1;
	if(hasDifferentAttrs(oldatt2,oldatt1)){
		while(!found){ //if 'found' then 'ind' is the newly chosen index to add.
			found=true; //assume we'll find in this iteration.
			i2=rand()% (oldatt2.size()-1); //draw a random attribute (last one is classifications thus -1)
			for(unsigned int k=0; k < oldatt1.size(); k++)
				if(oldatt1.at(k).at(0)==oldatt2.at(i2).at(0)){
					found=false;
					break; //to re-draw
				}
		}
	}
	
	if(i1==-1 && i2==-1)//abort action
		return;

	att1=new vector< vector<DataVar*> >();
	att2=new vector< vector<DataVar*> >();

	data1=new vector< vector<DataVar*> >();
	data2=new vector< vector<DataVar*> >();
	data1->resize(data->size());
	data2->resize(data->size());

	for(unsigned int k=0; i2!=-1 && k< oldatt1.size()-1; k++){
		//insert an attribute
		if(k!=(unsigned int)i2){
			att1->push_back(oldatt1.at(k));
		}else{
			att1->push_back(oldatt2.at(k));
		}
		//insert a data column
		int attInd1=findAttInd(att1->back());
		for(unsigned int j=0; j<data->size(); j++){
			data1->at(j).push_back(data->at(j).at(attInd1));
		}
	}
	for(unsigned int k=0; i1!=-1 && k< oldatt2.size()-1; k++){
		//insert an attribute
		if(k!=(unsigned int)i1){
			att2->push_back(oldatt2.at(k));
		}else{
			att2->push_back(oldatt1.at(k));
		}
		//insert a data column
		int attInd2=findAttInd(att2->back());
		for(unsigned int j=0; j<data->size(); j++){
			data2->at(j).push_back(data->at(j).at(attInd2));
		}
	}
	//insert last attribute line (classifications)
	if(i2!=-1)
		att1->push_back(oldatt1.back());
	if(i1!=-1)
		att2->push_back(oldatt2.back());

	//insert data classifications
	for(unsigned int j=0; j<data->size(); j++){
		if(i2!=-1)
			data1->at(j).push_back(data->at(j).back());
		if(i1!=-1)
			data2->at(j).push_back(data->at(j).back());
	}
	
	if(i2!=-1){
		this->forest[ind1]->clearAtrNdata();
		delete forest[ind1];
		forest[ind1]=new DecisionTree(att1,data1);
	}
	if(i1!=-1){
		this->forest[ind2]->clearAtrNdata();
		delete forest[ind2];
		forest[ind2]=new DecisionTree(att2,data2);
	}
}

/**
 * randomly choose an attribute that isn't in 't' and add it.
 */
void EvoForest::mutate(int tInd){
	vector< vector<DataVar*> > oldatt =forest[tInd]->getattributes();
	if(oldatt.size() == attributes->size())
		return; //can't enlarge tree
	srand((unsigned int)time(NULL));
	bool found=false; 
	int ind=0;
	while(!found){ //if 'found' then 'ind' is the newly chosen index to add.
		found=true; //assume we'll find in this iteration.
		ind=rand()% (attributes->size()-1); //draw a random attribute (last one is classifications thus -1)
		for(unsigned int k=0; k < oldatt.size(); k++)
			if(oldatt.at(k).at(0)==attributes->at(ind).at(0)){
				found=false;
				break; //to re-draw
			}
	}
	vector< vector<DataVar*> > *newatt =new vector<vector<DataVar*> >();
	vector< vector<DataVar*> > *newdata =new vector<vector<DataVar*> >();
	newdata->resize(data->size());
	for(unsigned int i=0; i<oldatt.size()-1; i++){
		newatt->push_back(oldatt.at(i));
		int loc=findAttInd(newatt->back());
		for(unsigned int j=0; j< data->size(); j++)
			newdata->at(j).push_back(data->at(j).at(loc));
	}
	newatt->push_back(attributes->at(ind));
	newatt->push_back(attributes->back());
	for(unsigned int j=0; j< data->size(); j++){
		newdata->at(j).push_back(data->at(j).at(ind));
		newdata->at(j).push_back(data->at(j).back());
	}
	this->forest[tInd]->clearAtrNdata();
	delete forest[tInd];
	forest[tInd]=new DecisionTree(newatt,newdata);
}

/**
 * sort forest according to DecisionTree::getFitness()
 *
 * good fitness <==> low error <==> will be on higher indices in forest
 */
void EvoForest::boubleSort(){
	for(int i=size-1; i > 0; i--)
		for(int j=0; j < i; j++){
			if(forest[j]->getFitness() < forest[j+1]->getFitness()){ 
				DecisionTree *tmp = forest[j];
				forest[j]=forest[j+1];
				forest[j+1] = tmp;
			}
		}
}
/**
 * binary search for and index 'i' such that arr[i+1] >= num > arr[i]
 * precondition: num <= arr[size-1]
 */
int EvoForest::binSearch(float num, float* arr,unsigned int size){
	//cout << "num=" <<num << " arr[last]=" << arr[size-1] << endl;
	int from=0;
	int to=size-1;
	if(from==to) return from;
	int middle=to/2;
	if(arr[to]<= num)
		return to;
	for(;;){
		if((middle+1>=(int)size) || (arr[middle+1]>= num && arr[middle]<num))
			return middle;
		if(arr[middle]< num){
			from=middle;
		}else{
			to=middle;
		}
		middle= (from+to)/2;
	}
}

/**
 * selects trees for next generation
 * using linear ranking with s=2
 */
void EvoForest::select(){
	boubleSort();
	float* rank=new float[size];
	float sumRanks=0;
	for(unsigned int i=0; i<size; i++){
		rank[i]= (float)2*i / (size*(size-1)); //here is the linearRanking formula: 2*rank / (size*(size-1))
		sumRanks+=rank[i];
	}
	rank[0]=rank[0]/sumRanks;
	/*this orders 'rank' so that higher ranks has bigger difference between then 
	 so that trees with better percision has more chances to be chosen to next generation when randomly
	 selecting a number (0,1]
	 for example: [0.01,0.015,0.023,0.04,0.06,...,0.6,0.77,1]
	 diffrences between elements increase as array indices increase*/
	for(unsigned int i=1; i<size; i++){		
		rank[i]= rank[i-1] + rank[i]/sumRanks;
	}
	DecisionTree** newForest=new DecisionTree*[size];
	
	srand((unsigned int)time(NULL));
	for(unsigned int i=0; i<size; i++){ //draw trees to next generation
		float r = (   (float)rand() / ((float)(RAND_MAX)+(float)(1)) );
		int ind=binSearch(r,rank,size);
		newForest[i]=new DecisionTree(*forest[ind]);
	}
	for(unsigned int i=0; i<size; i++) //clear old forest
		this->forest[i]->clearAtrNdata();
	delete[] forest;
	forest=newForest; //forest now points to the new one.
	delete[] rank;
}

/*
 * for each generation: 
 * crossover two neighbouring trees with high probability
 * mutate trees with low probability
 * select trees for next generations
 */
void EvoForest::evolution(int generations){
	srand((unsigned int)time(NULL));
	for(int i=0; i<generations; i++){
		cout<< "calculating generation " << i+1 << endl;
		for(unsigned int j=0; j<size; j+=2){
			float r = (   (float)rand() / ((float)(RAND_MAX)+(float)(1)) );
			if(r<0.9)
				crossover(j,j+1);//forest[j],forest[j+1]);
			r = (   (float)rand() / ((float)(RAND_MAX)+(float)(1)) );
			if(r<0.05){
				mutate(j);//forest[j]);
				mutate(j+1);//forest[j+1]);
			}
		}
		select();
	}
}