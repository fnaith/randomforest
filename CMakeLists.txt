cmake_minimum_required(VERSION 2.6)

project(randomForest CXX)

get_filename_component(REPO_DIR "${PROJECT_SOURCE_DIR}" DIRECTORY)

# randomForest setting
set(randomForest 
src/DataVar.cpp 
src/DecisionTree.cpp 
src/EvoForest.cpp 
src/Excptn.cpp 
src/DataVar.cpp 
src/RandomForest.cpp 
src/main.cpp 
)

# mingw32 setting
if (${CMAKE_GENERATOR} MATCHES "^MinGW Makefiles")
  set(GCC_CXX_F "-fmax-errors=1 -fno-strict-aliasing -ftrapv -fstack-protector")
  set(GCC_CXX_W "-Werror -Wall -Wextra -Wformat=2 -Wstrict-aliasing=2 -Wcast-qual -Wcast-align -Wwrite-strings -Wconversion -Wfloat-equal -Wpointer-arith -Wswitch-enum")
  set(CMAKE_CXX_FLAGS_DEBUG "-g -pg -O0")
  set(CMAKE_CXX_FLAGS_RELEASE "-O3 -D_FORTIFY_SOURCE=2")
  set(CMAKE_CXX_FLAGS "-ansi ${GCC_C_F} ${GCC_C_W}")
  add_executable(proj ${randomForest})
# vc-20xx setting
elseif (${CMAKE_GENERATOR} MATCHES "^Visual Studio")
  set(CMAKE_CXX_FLAGS_DEBUG "/MDd /Od /Zi")
  set(CMAKE_CXX_FLAGS_RELEASE "/MD /Ox")
  set(CMAKE_CXX_FLAGS "/EHsc /WX /W4 /wd4996")
  add_executable(proj ${randomForest})
# generator error
else()
  message(FATAL_ERROR "${CMAKE_GENERATOR} is not allowed, you should add that to randomForest/CmMakeLists.txt")
endif()
