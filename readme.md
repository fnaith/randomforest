Welcome to randomForest!
========================

randomForest is an cross platform C++ software tool implements Random Forests with Genetic Algorithms developed by Guy, OR&ROTEM.

Build randomForest
==================

There are many ways to build randomForest.

For CMake's user :

1. If you use MinGW32, double click build/mingw32/build.bat and use 'mingw32-make' at same folder.

2. If you use Visual Studio 2010 or Visual Studio 2012, double click build/vc20(10/12)/build.bat and use IDE as usual.

3. If you use other complier, please read that simple CMakeLists.txt and add configure by yourself.

For non-CMake's user :

Add all files in folder src and folder h to your project.

Dependencies
============

None.

Licensing
=========

Not Decided Yet!
