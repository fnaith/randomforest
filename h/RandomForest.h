/*
 * RandomForest.h
 *
 *  Created on: Jun 28, 2010
 *      Author: operry5
 */

#ifndef RANDOMFOREST_H_
#define RANDOMFOREST_H_

#include "DecisionTree.h"

/*
 * WARNING! 
 * this class allows a user to initialize either from file or by giving attribites & data pointers.
 * as such, it may corrupt outside attributes & data if not handeled with care.
 *
 * if initialized with pointers. the destructor will not delete these structs!
 */
class RandomForest{
private:
	float minPercentage;
	DecisionTree** forest;
	unsigned int size; //forest size
	vector< vector<DataVar*> > *attributes; //data-table headers and possible values
	vector< vector<DataVar*> > *data; //the actual numbers (the data table)
	bool errorOnBuild,constructedWithPtrs; //signal abortion if error occured on build.
	void readData(const std::string& attrFile, const std::string& dataFile);
	void setAttrs(const std::string& attrFile);

	RandomForest(const RandomForest& other);
	RandomForest& operator=(const RandomForest& t);

	vector<DataVar*>& setToEval(vector<DataVar*>* evalRec,unsigned int treeInd) const; //helps eval
public:
	inline const vector< vector<DataVar*> > getattributes() {return *(this->attributes);}
	RandomForest(unsigned int size, unsigned int attrsPerTree, const std::string& attrFile, const std::string& dataFile);
	RandomForest(unsigned int size, unsigned int attrsPerTree, vector< vector<DataVar*> > *attrs, vector< vector<DataVar*> > *data);
	virtual ~RandomForest();

	string toString() const;
	string eval(vector< vector<DataVar*> > *evaldata) const;
	string eval(const std::string& dataFile) const;
	inline int getNclasses() const { return (int)(attributes->back().size()-1);} 
};

#endif /* RANDOMFOREST_H_ */
