/*
 * Excptn.h
 *
 *  Created on: Mar 3, 2010
 *      Author: operry5
 */

#ifndef EXCPTN_H_
#define EXCPTN_H_
#include <exception>

//class exception;
class Excptn /*: public exception*/{
private:
	std::string _msg;
public:
	Excptn(const std::string msg);
	virtual const char* what() const throw();
	virtual ~Excptn();
};
#endif /* EXCPTN_H_ */
