#pragma once

#include "DecisionTree.h"

class EvoForest{
private:
	float minPercentage;
	DecisionTree** forest;
	unsigned int size; //forest size
	vector< vector<DataVar*> > *attributes; //data-table headers and possible values
	vector< vector<DataVar*> > *data; //the actual numbers (the data table)
	/*do not use the following methods (3)*/
	EvoForest(void);
	EvoForest(const EvoForest& other);
	EvoForest& operator=(const EvoForest& t);

	void crossover(int ind1,int ind2);
	void mutate(int tInd);//DecisionTree* t);
	float fitness(int idx);
	void select();
	void evolution(int generations); //do evolution (calls above (4) methods)

	vector<DataVar*>& setToEval(vector<DataVar*>* evalRec,unsigned int treeInd) const;
	unsigned int findAttInd(vector<DataVar*> _att);
	bool hasDifferentAttrs(vector< vector<DataVar*> > &att,vector< vector<DataVar*> > &otherAtt);
	void boubleSort(); //sorts forest according to fitness func. used in 'select()'
	int binSearch(float num, float* arr,unsigned int size);
public:
	virtual ~EvoForest();
	EvoForest(int generations, unsigned int _size, unsigned int attrsPerTree, 
		vector< vector<DataVar*> > *attrs, vector< vector<DataVar*> > *data);
	inline const vector< vector<DataVar*> > getattributes() {return *(this->attributes);}
	string toString() const;
	string eval(vector< vector<DataVar*> > *evaldata) const;
	string eval(const std::string& dataFile) const;
	inline int getNclasses() const { return (int)(attributes->back().size()-1);}
};
