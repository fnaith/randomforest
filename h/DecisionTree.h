/*
 * DecisionTree.h
 *
 *  Created on: Nov 24, 2009
 *      Author: operry5
 */

#ifndef DecisionTree_H_
#define DecisionTree_H_

#include <vector>
#include <stdio.h>
#include "DataVar.h"

using namespace std;

/*
 * WARNING! 
 * this class allows a user to initialize either from file or by giving attribites & data pointers.
 * as such, it may corrupt outside attributes & data if not handeled with care.
 *
 * if initialized with pointers. the destructor will not delete these structs!
 */

class DecisionTree{
private:
	int minleaf,minparent,nclasses,nodenumber,m;
	int *parent,**children,*nodesize,*cutvar,**classcount, *yfitnode;
//	int** cost;
	float *cutpoint,*nodeprob,*resuberr,**classprob,*impurity;/*,*iscat*///,*prior;

	//fields here
	vector< vector<DataVar*> > *attributes; //data-table headers and possible values
	vector< vector<DataVar*> > *data; //the actual numbers (the data table)
	bool errOnBuild,constructedWithPtrs; 
	float fitness; //for Evolution.


	void readData(const std::string& attrFile, const std::string& dataFile);
	void setAttrs(const std::string& attrFile);
	void makeTree();

	void quickSort(float arr[], int indices[], int left, int right);
	void boubleSort(float arr[], int indices[],int size);
	int partition(float arr[], int indices[], int left, int right, int pivotInd);
	void swap(float arr[], int indices[], int first,int second);

	/*DO NOT USE THESE METHODS(1):*/
	DecisionTree();

public:
	DecisionTree(const DecisionTree& other);
	DecisionTree& operator=(const DecisionTree& t);

	inline const vector< vector<DataVar*> > getattributes() {return *(this->attributes);}
	float eps(float num);
	void sort(float arr[], int indices[],int size);
	float gdi(float p[],int length);
	float* RCcritval(float x[],bool* c[],int* Ccum[], vector<int> rows, /*float xcat,*/
					float pratio[],float Pt, float bestcrit,int minleaf, int Nnode, int nclasses);/*critfun, only for reg*/
	void mergeLeaves();
	float* risk();
	void prune(bool* doprune);

	DecisionTree(const std::string& attrFile, const std::string& dataFile);
	DecisionTree(vector< vector<DataVar*> > *attrs, vector< vector<DataVar*> > *data);
	virtual ~DecisionTree();
	void clearAtrNdata(); //for forest use. this clears (shallow) attributes & data

	string toString() const;
	string getnodeerr(int ind) const;
	string getclasscount(int ind) const;
	string eval(vector< vector<DataVar*> > *evaldata) const;
	string eval(const std::string& dataFile) const;
	//to use with RandomForest eval:
	string getEvalData(vector<DataVar*> *toEval, int* classification, float* nodePercentage, float* nodeErr);
	
	float setFitness();
	float getFitness();

};

#endif /* DecisionTree_H_ */
