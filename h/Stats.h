/*
 * Stats.h
 *
 *  Created on: Mar 9, 2010
 *      Author: operry5
 */

#ifndef STATS_H_
#define STATS_H_

#include <math.h>
#include <string.h>
#include <vector.h>

class DataVar;

float infoT(vector<int> partition,int total);
float gainRatio(const vector<DataVar*> column,const vector<DataVar*> classifications,
		const vector<DataVar*> attrVals, const vector<DataVar*> classVals,
		vector<int> recordIndxs,float info_T);
//data.at(attrsLeft.at(i)),attrs.at(attrsLeft.at(i)),
	//				attrs.at(attrs.size()-1),recordIndxs);

#endif /* STATS_H_ */
