/*
 * DataVar.h
 *
 *  Created on: Jun 28, 2010
 *      Author: operry5
 */

#ifndef DATAVAR_H_
#define DATAVAR_H_

#include <string>
#include <stdio.h>

using namespace std;

typedef enum DataVarType{
		NUMERIC=0,
		STRING
}DataVarType;

class DataVar{
private:
	string strVal;
	float numVal;  //only one of which is initialized
	DataVarType type;
	DataVar();

	void addExp(string& str,string& left,float num,bool isNegative);
	void addFraction(string& str,string& left,float num,bool isNegative);
public:
	DataVar(float num); //creates a new numeric DataVar with numVal=num
	DataVar(DataVarType type,string& str); //creates a new DataVar with given type & data taken from given string
	virtual ~DataVar();

	void setAsString(string& str); //instead of tokenizeString();
	void setAsNum(float _num);
	void tokenize(string& str);

	DataVarType getType(){return type;}
	void setType(DataVarType _type);
	float getNumVal();
	string getStrVal();

};
/*
 * the following structures are here for future possibility of extending data type to non-continuous

typedef union DataValue{
	string* str;
	float* num;
}DataValue;


typedef struct DataVar{
	DataVarType type;
	DataValue dval;
}DataVar;

DataVar* makeDataVar(float* num);
DataVar* tokenizeString(string& str);
DataVar* tokenize(string& str);
*/
#endif /* DATAVAR_H_ */
